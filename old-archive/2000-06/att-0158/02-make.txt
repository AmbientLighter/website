for subdir in lib sanei backend frontend doc tools; do		\
  target=`echo all-recursive | sed s/-recursive//`; \
  echo making $target in $subdir;	\
  (cd $subdir && make $target)	\
   || case "" in *k*) fail=yes;; *) exit 1;; esac; \
done && test -z "$fail"
making all in lib
make[1]: Entering directory `/tmp/sane-1.0.2/lib'
rm -f .libs/sigprocmask.lo
gcc -c -DHAVE_CONFIG_H -I. -I. -I../include/sane -I../include -D_GNU_SOURCE -DPATH_SANE_CONFIG_DIR=/usr/local/etc/sane.d -DPATH_SANE_DATA_DIR=/usr/local/share -DV_MAJOR=1 -DV_MINOR=0 -g -O2 -W -Wall -DSCSIBUFFERSIZE=131072 sigprocmask.c  -fPIC -DPIC -o .libs/sigprocmask.lo
make[1]: Leaving directory `/tmp/sane-1.0.2/lib'
