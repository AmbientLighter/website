.TH sane-canon 5 "03 May 2001"
.IX sane-canon
.SH NAME
sane-canon - SANE backend for Canon SCSI scanners
.SH DESCRIPTION
The
.B sane-canon
library implements a SANE (Scanner Access Now Easy) backend that
provides access to the following Canon flatbed and film scanners:
.PP
.RS
CanoScan 300
.br
CanoScan 600
.br
CanoScan FB620S
.br
CanoScan FB1200S
.br
CanoScan FS2700F                                                          
.br
CanoScan FS2710                                                          
.br
.RE
.PP
No parallel port and USB scanners are supported and there are no plans to
support them in the future.
.PP
IMPORTANT: this is alpha code. Don't expect this to work
correctly. Many functions are missing, others contain errors. In some
cases, your computer might even hang. It cannot be excluded (although
I consider it extremely unprobable) that your scanner will be
damaged.
.PP
That said, TESTERS ARE WELCOME. Send your bug reports and comments to
Manuel Panea <mpd@rzg.mpg.de>; for questions concerning the FB620
contact Mitsuru Okaniwa <m-okaniwa@bea.hi-ho.ne.jp>, for the FS2710
Ulrich Deiters <ukd@xenon.pc.uni-koeln.de>.
.PP

.SH TIPS (FS2700)
.PP
Scanning either slides or negatives has been found to require rather
large gamma corrections of about 2.2 to 2.4 (same value for red, green, 
and blue). Also, for slides, using "Auto Exposure" helps a lot.
.PP
The "Auto Exposure" function for slides makes the scanner do a first
pass on the film to determine the Highlight and Shadow point
values. The "Auto Focus" function triggers yet another pass to
determine the focus value. After that, the real scanning pass takes
place. The "Auto Exposure" function is not available for negatives yet.
.PP
Even with "Auto Focus" turned on, the scanned image is often a bit too
blurred. Using the GIMP to do a "Filter->Enhance->Sharpen" at about 40
to 60 improves the image considerably. It is likely that a sharpen filter
will be included in a future version of this backend.
.PP
The "native" scanner resolutions of the 2700F are the integer
sub-multiples of 2720 dpi, i.e. 1360, 680, 340 and 170. Scanning at
other resolutions is possible, but there will be noticeably more jaggies
in the image than when using one of the "native" resolutions.
.PP

.SH TIPS (FS2710)
.PP
Gamma corrections are done not by the scanner, but by the backend.
The scanner is always run in 12-bit mode. In "color" mode the image
data are corrected for gamma, shadow point, etc., and then truncated
to 8-bit intensities; the default gamma value is 2.5. In "raw" mode the
image data are exported without corrections as 16-bit intensities; this
mode can be recommended if extensive adjustments have to be made to a
picture (and if the frontend can handle 16-bit intensities).
.PP
Negatives are handled by simple color inversion and may require manual
removal of blue discoloration.
.PP
.SH FILES
.TP
.I @LIBDIR@/libsane-canon.a
The static library implementing this backend.
.TP
.I @LIBDIR@/libsane-canon.so
The shared library implementing this backend (present on systems that
support dynamic loading).
.SH ENVIRONMENT
.TP
.B SANE_DEBUG_CANON
If the library was compiled with debug support enabled, this
environment variable controls the debug level for this backend.  Higher
debug levels increase the verbosity of the output. 

Example: 
export SANE_DEBUG_CANON=4

.SH "SEE ALSO"
sane\-scsi(5)
.br
http://www.rzg.mpg.de/~mpd/sane/
.br
doc/canon.install2700F.txt (installation of a CanoScan 2700F
.br
.SH AUTHOR
Helmut Koeberle, Manuel Panea, and Markus Mertinat; FB620S support
by Mitsuru Okaniwa; FS2710 support by Ulrich Deiters
.br
Man page by Henning Meier-Geinitz (mostly based on canon.README)
