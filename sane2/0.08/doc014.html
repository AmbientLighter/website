<html><body>
<a href="doc015.html"><img src=../icons/next.gif alt="Next"></a>
<a href="doc000.html"><img src=../icons/up.gif alt="Up"></a>
<a href="doc013.html"><img src=../icons/previous.gif alt="Previous"></a>
<a href="doc000.html"><img src=../icons/contents.gif alt="Contents"></a>
<a href="doc019.html"><img src=../icons/index.gif alt="Index"></a>
<hr>

<title>Well-Known Options</title>
<h2><a name="s4.5">4.5 Well-Known Options</a></h2><a name="i118">

<p>While most backend options are completely self-describing, there are
cases where a user interface might want to special-case the handling
of certain options.  For example, the scan area is typically defined
by four options that specify the top-left and bottom-right corners of
the area.  With a graphical user interface, it would be tedious to
force the user to type in these four numbers.  Instead, most such
interfaces will want to present to the user a preview (low-resolution
scan of the full scanner surface or a high(er) resolution scan of a subpart
of the scanner surface) and let the user pick the scan area by
dragging a rectangle into the desired position.  For this reason, the
SANE API specifies a small number of option names that have
well-defined meanings.

<p><h3><a name="s4.5.1">4.5.1 Option Number Count</a></h3><a name="i119">

<p>Option number 0 has an empty string as its name.  The value of this
option is of type <tt>SANE_TYPE_INT</tt> and it specifies the total
number of options available for a given device (the count includes
option number 0).  This means that there are two ways of counting the
number of options available: a frontend can either cycle through all
option numbers starting at one until
<tt>sane_get_option_descriptor()</tt> returns <tt>NULL</tt>, or a
frontend can directly read out the value of option number 0.

<p><h3><a name="s4.5.2">4.5.2 Scan Resolution Options</a></h3><a name="i120"><a name="i121">

<p>Option <tt>resolution</tt> is used to select the resolution at which an
image should be acquired.
<font color="darkgreen">
When the backend wants to allow different
values for x- and y-resolution it has to define the options
<tt>x-resolution</tt> and <tt>y-resolution</tt>. Note that only
the option <tt>resolution</tt> <b>or</b> the options
<tt>x-resolution</tt> <b>and</b> <tt>y-resolution</tt> may be active at the
same time.
</font>

<p>The type of this option is either <tt>SANE_TYPE_INT</tt> or
<tt>SANE_TYPE_FIXED</tt>.  The unit is <tt>SANE_UNIT_DPI</tt> (dots/inch).

<p>These options are not mandatory, but if a backend does support them, it
must implement them in a manner consistent with the above definition.

<p><h3><a name="s4.5.3">4.5.3 Preview Mode Option</a></h3><a name="i122">

<p>The boolean option <tt>preview</tt> is used by a frontend to inform the
backend when image acquisition should be optimized for speed, rather
than quality (``preview mode'').  When set to <tt>SANE_TRUE</tt>,
preview mode is in effect, when set to <tt>SANE_FALSE</tt> image
acquisition should proceed in normal quality mode.  The setting of
this option <em>must not</em> affect any other option.  That is, as
far as the other options are concerned, the preview mode is completely
side effect free.  A backend can assume that the frontend will take
care of appropriately setting the scan resolution for preview mode
(through option <tt>resolution</tt>).  A backend is free to override the
<tt>resolution</tt> value with its own choice for preview mode, but it
is advised to leave this choice to the frontend wherever possible.
<font color="darkgreen">
When the <tt>preview</tt> option is set the backend should transfer
the image in frame type <tt>SANE_FRAME_RAW</tt> if possible.
</font>

<p>This option is not mandatory, but if a backend does support it, it
must implement it in a manner consistent with the above definition.

<p><h3><a name="s4.5.4">4.5.4 Scan Area Options</a></h3><a name="i123">

<p>The four most important well-known options are the ones that define
the scan area.  The scan area is defined by two points (x/y coordinate
pairs) that specify the top-left and the bottom-right corners.  This
is illustrated in Figure <a href="doc014.html#f5">5</a>.  Note that the origin of the
coordinate system is at the top-left corner of the scan surface as
seen by the sensor (which typically is a mirror image of the scan
surface seen by the user).  For this reason, the top-left corner is
the corner for which the abscissa and ordinate values are
simultaneously the <em>smallest</em> and the bottom-right corner is the
corner for which the abscissa and ordinate values are simulatenously
the <em>largest</em>.  If this coordinate system is not natural for a
given device, it is the job of the backend to perform the necessary
conversions.
<p><a name="f5"></a>
  <center>
    
    <img width=330 height=306 src="img004.gif">
    <p><center>Figure 5: Scan area options</center>
    
  </center>
<p>

<p>The names of the four options that define the scan area are given in
the table below:
<center>
<table cellpadding=0 cellspacing=0>
<tr valign=top>
<td colspan=1 align=left nowrap>
<b>Name</b> </td>
<td colspan=1 align=left nowrap> <b>Description</b> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>tl-x<a name="i124"></tt> </td>
<td colspan=1 align=left nowrap> Top-left x coordinate value </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>tl-y<a name="i125"></tt> </td>
<td colspan=1 align=left nowrap> Top-left y coordinate value </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>br-x<a name="i126"></tt> </td>
<td colspan=1 align=left nowrap> Bottom-right x coordinate value </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>br-y<a name="i127"></tt> </td>
<td colspan=1 align=left nowrap> Bottom-right y coordinate value </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
</td></tr>
</table>
</center>
There are several rules that should be followed by front and backends
regarding these options:
<ul>

<p><li>Backends must attach a unit of either pixels
  (<tt>SANE_UNIT_PIXEL</tt>) or millimeters (<tt>SANE_UNIT_MM</tt>) to
  these options.  The unit of all four options must be identical.

<p><li>Whenever meaningful, a backend should attach a range or a
  word-list constraint to these options.

<p><li>A frontend can determine the size of the scan surface by first
  checking that the options have range constraints associated.  If a
  range or word-list constraints exist, the frontend can take the
  minimum and maximum values of one of the x and y option
  range-constraints to determine the scan surface size.

<p><li>A frontend must work properly with any or all of these options
  missing.
  
<font color="darkgreen">
<li>A frontend may temporarily set the values in a way that
<tt>tl-x</tt> is larger than <tt>br-x</tt> and <tt>tl-y</tt> is larger than
<tt>br-y</tt>.
</font>

<p></ul>

<p><font color="darkgreen">
These options are not mandatory, but if a backend does support them, it
must implement them in a manner consistent with the above definition.
</font>

<p><font color="darkgreen">
<h3><a name="s4.5.5">4.5.5 Depth Option</a></h3><a name="i128">
Option <tt>depth</tt> is used to select the image depth in bits/sample
in multi bit mode (e.g. for 24 bit RGB mode this value must be 8).
The type of this option is <tt>SANE_TYPE_INT</tt>.
The unit is <tt>SANE_UNIT_BIT</tt>. For 1 bit modes
(Lineart or Halftone) this option has to be inactive.
For selection of 1 bit modes (Lineart or Halftone) the
backend should use the well-known option <tt>mode</tt>.
 
This option is not mandatory, but if a backend does support it, it
must implement it in a manner consistent with the above definition.
</font>
 
<font color="darkgreen">
<h3><a name="s4.5.6">4.5.6 Scan Mode Options</a></h3><a name="i129">
The option <tt>mode</tt> defines a <tt>SANE_CONSTRAINT_STRING_LIST</tt>
of type <tt>SANE_TYPE_STRING</tt>.
It is used to select the scanmode (e.g. Color or Gray).
Well known modes are: <tt>Color</tt>, <tt>Gray</tt>, <tt>Halftone</tt>
and <tt>Lineart</tt>. <tt>Color</tt> and <tt>Gray</tt> are multi bit
modes (8 or 16 bits/sample), <tt>Halftone</tt> and <tt>Lineart</tt>
are single bit modes. When well-known scan modes are used,
a frontend is able to automatically decide which mode is appropriate
for a specific task, e.g the mode <tt>Gray</tt>
for scanning a fax.
 
This option is not mandatory, but if a backend does support it, it
must implement it in a manner consistent with the above definition.
</font>
 
<font color="darkgreen">
<h3><a name="s4.5.7">4.5.7 Scan Source Options</a></h3><a name="i130">
The option <tt>source</tt> is used to select the scan source
(e.g. Automatic Document Feeder).
It defines a <tt>SANE_CONSTRAINT_STRING_LIST</tt>
of type <tt>SANE_TYPE_STRING</tt>.
Well known sources are: <tt>Flatbed</tt>, <tt>Transparancy Adapter</tt> and
<tt>Automatic Document Feeder</tt>.
 
This option is not mandatory, but if a backend does support it, it
must implement it in a manner consistent with the above definition.
</font>

<p><font color="darkgreen">
<h3><a name="s4.5.8">4.5.8 Threshold Option</a></h3><a name="i131">
The option <tt>threshold</tt> is used to define the threshold
for Lineart and maybe Halftone mode. In multi bit modes
this option should be set inactive.
The type of this option is <tt>SANE_TYPE_FIXED</tt>.
The unit is <tt>SANE_UNIT_PERCENT</tt>. The value range
should be 0.0...100.0 if possible.
It defines the minimum intensity to get a white point / full intensity.

<p>The backend has to scale the values in the following way:
A value of 0.0 means all pixels get white / full intensity.  A value of 50.0
means intensities brighter than medium gray get white / full intensity.  A
value of 100.0 means all pixels get black. If the
scanner is not able to cover the full range the backend has to define a
reduced value range (e.g. 30...70 percent).

<p>This option is not mandatory, but if a backend does support it, it
must implement it in a manner consistent with the above definition.
</font>

<p><font color="darkgreen">
<h3><a name="s4.5.9">4.5.9 Gamma Table Options</a></h3><a name="i132">
The <tt>gamma-table</tt> option defines a <tt>SANE_CONSTRAINT_RANGE</tt>
of the type <tt>SANE_TYPE_INT</tt> which represents the gamma correction
table for gray.  In color mode the <tt>gamma-table</tt> may be used to set
a common gamma correction for red, green and blue. The options
<tt>red-gamma-table</tt>, <tt>green-gamma-table</tt> and <tt>blue-gamma-table</tt>
are used in color mode to set a gamma correction for each color
separately. In color mode the backend is free to use only the
<tt>gamma-table</tt> option, only the <tt>red-</tt>, <tt>green-</tt> and
<tt>blue-gamma-table</tt> or all four options. When all four options
are used then the color tables should do a gamma correction with
the same input and output bit depth and the gray gamma table should
reduce (if necessary) the bit depth from the scanner internal
bit depth to the output bit depth. This should e.g. look like this:
<pre>
red_value   = gamma-table(red-gamma-table(value))
green_value = gamma-table(green-gamma-table(value))
blue_value  = gamma-table(blue-gamma-table(value))
</pre>

<p>The backend should not use the gamma tables to emulate other functions or options
like highlight, shadow, contrast, brightness, threshold, analog_gamma.
These functions are common for all backends and should be added to the frontend
or a meta-backend.

<p>It is also discouraged to emulate gamma tables in the backend. The backend
should disable (or not define) this option when the scanner does not support
gamma tables in hardware.
 
This option is not mandatory, but if a backend does support it, it
must implement it in a manner consistent with the above definition.
</font>

<p><font color="darkgreen">
<h3><a name="s4.5.10">4.5.10 Analog Gamma</a></h3><a name="i133">
The option <tt>analog-gamma</tt> is used to define the gamma value 
for an analog gamma function of the scanner in multi bit modes.
In 1 bit modes this option should be set inactive.
The type of this option is <tt>SANE_TYPE_FIXED</tt>.
The unit is <tt>SANE_UNIT_NONE</tt>. The value range
can be defined by the backend as supported. The values
have to be positive. A gamma value of 1.0 means that
the gamma correction has no effect. A value larger than
1.0 increases the brightness of the image.
In color mode there also can be options <tt>analog-gamma-red</tt>,
<tt>analog-gamma-green</tt> and <tt>analog-gamma-blue</tt>.
It is not allowed to emulate an analog gamma function by
a digital gamma table. The backend has to disable (or not
define) this option when the scanner does not support an
analog (hardware) gamma function.

<p>When analog gamma, highlight and shadow functions are available
at the same time then the backend author has to care about the order
in which the functions are implemented in the scanner hardware.
The SANE standard expects that changing the analog gamma value
has no effect on the shadow and highlight function. When the
analog gamma function is executed in the scanner hardware
before the shadow and highlight functions then the backend
should do a compensation. For this the shadow and highlight
values have to be gamma corrected with the relevant analog gamma value.

<p>This option is not mandatory, but if a backend does support it, it
must implement it in a manner consistent with the above definition.
</font>

<p><font color="darkgreen">
<h3><a name="s4.5.11">4.5.11 Shadow Option</a></h3><a name="i134">
The option <tt>shadow</tt> is used to define the shadow level / black point level.
The type of this option is <tt>SANE_TYPE_FIXED</tt>.
The unit is <tt>SANE_UNIT_PERCENT</tt>. The value range
should be 0.0...100.0 if possible.
It is used to define the maximum intensity level that creates an image data value
of 0 (black).

<p>The backend has to scale the values in the following way:
A value of 0.0 means that the sensitivity range is not reduced, only the
minimum intensity produces an image data value of 0 (black).
A value of 50.0 means that that a medium intensity and everything that is darker
produces an image data value of 0 (black).
A value of 100.0 means the sensitivity range is reduced to 0, all image
data values are 0 (black).  If the scanner is not able to
cover the full range the backend has to define a reduced
value range (e.g. 30...70 percent).
In color mode there can be options <tt>shadow-red</tt>, <tt>shadow-green</tt>
and <tt>shadow-blue</tt>, in this case the <tt>shadow</tt> function has to be disabled.
It is not allowed to emulate a shadow function by
a digital gamma table. The backend has to disable (or not
define) this option when the scanner does not support an
analog (hardware) shadow function.

<p>This option is not mandatory, but if a backend does support it, it
must implement it in a manner consistent with the above definition.
</font>

<p><font color="darkgreen">
<h3><a name="s4.5.12">4.5.12 Highlight Option</a></h3><a name="i135">
The option <tt>highlight</tt> is used to define the highlight level / white point level.
The type of this option is <tt>SANE_TYPE_FIXED</tt>.
The unit is <tt>SANE_UNIT_PERCENT</tt>. The value range
should be 0.0...100.0 if possible.
It is used to define the minimum intensity level that creates the maximum possible
image data value (white/full intensity). 

<p>The backend has to scale the values in the following way:
A value of 0.0 means the sensitivity range is reduced to 0, all image
data have maximum value (white / full intensity).
A value of 50.0 means that a medium intensity and everything that is brighter
produces the maximum possible image data value (white / full intensity).
A value of 100.0 means that the sensitivity range is not reduced, only the
maximum intensity produces an image data with maximum possible value (white / full intensity).
If  the scanner is not able to cover the full range the backend has to define a reduced
value range (e.g. 30...70 percent).
In color mode there can be options <tt>highlight-red</tt>, <tt>highlight-green</tt>
and <tt>highlight-blue</tt>, in this case <tt>highlight</tt> has to be disabled.
It is not allowed to emulate a highlight function by
a digital gamma table. The backend has to disable (or not
define) this option when the scanner does not support an
analog (hardware) highlight function.

<p>This option is not mandatory, but if a backend does support it, it
must implement it in a manner consistent with the above definition.
</font>

<p><font color="darkgreen">
<h3><a name="s4.5.13">4.5.13 Lamp Options</a></h3><a name="i136"><a name="i137">

<p>The options <tt>lamp-on</tt> and <tt>lamp-off</tt> are button options
(<tt>SANE_TYPE_BUTTON</tt>) and don't have a unit (<tt>SANE_UNIT_NONE</tt>).

<p>Option <tt>lamp-on</tt> is used to turn on the lamp of the scanner while
<tt>lamp-off</tt> turns it off.

<p>These options are not mandatory, but if a backend does support them, it
must implement them in a manner consistent with the above definition.
</font>

<p><font color="darkgreen">
<h3><a name="s4.5.14">4.5.14 Scanner Button Options</a></h3><a name="i138">
Some scanners have buttons which state can be read by the scanner driver. 
It is necessary to implement a locking function for the buttons
because it is possible that several frontends try to connect to the
same backend/scanner at the same time. Imagine what could happen
when no locking would be implemented:
Five people have started a scanning application which is connected
via network to the scanner you want to use. You start a frontend,
put a paper to the scanner and press the scan-button on the scanner.
The scanner does scan three times (because three frontends asked the
button status when you pressed the button). For three people the
image is saved to the harddisk, but it is not sure that your
frontend did scan the image.

<p>A backend that does make available the scanner-buttons has to
implement the following options:<br>
<tt>scanner-buttons-lock</tt> is of type <tt>SANE_TYPE_BOOL</tt>, default = <tt>SANE_FALSE</tt><br>
<tt>scanner-buttons-status</tt> is of type <tt>SANE_TYPE_INT</tt>, default = 0<br>
<tt>scanner-buttons-status-update</tt> is of type <tt>SANE_TYPE_BUTTON</tt><br>
When setting these options the backend does not set <tt>SANE_INFO_RELOAD_OPTIONS</tt>
or <tt>SANE_INFO_RELOAD_PARAMS</tt> if not explictly defined.

<p>A frontend has to disable the usage of the scanner-buttons by default. This is important
because other frontends will not be able to use the buttons when the button-functions are locked.
Another important thing is that some scanners do not turn off their lamp when the driver
does frequently talk to the scanner (what is done when reading the button status from the scanner).

<p><ul>

<p><li>
A frontend that wants to read the button status has to lock the
button functions at first. For this it does set the option
<tt>scanner-buttons-lock</tt> to <tt>SANE_TRUE</tt>.
While setting the value of option <tt>scanner-buttons-lock</tt> to <tt>SANE_TRUE</tt>
the backend does check if a lockfile (e.g. ``backend''-buttons.lock) does exist.
The lockfile has to be placed in a directory where every user has read and write access to.

<p>  <ul>
  <li>
  If the lockfile does not exist then the backend creates the lockfile and writes the
  process ID (PID) of the backend to the file. Button access is allowed: the value
  of option <tt>scanner-buttons-lock</tt> is set to <tt>SANE_TRUE</tt>
  <li>
  If the file does exist and the backend PID is not the file PID then the
  backend has to check if the process with the PID stored in the lockfile
  still is running. If yes then the button access is not allowed: the value of
  option <tt>scanner-buttons-lock</tt> is set to <tt>SANE_FALSE</tt>. If not then the lockfile
  is recreated and the PID of the backend is stored in the lockfile, button
  access is allowed: the value of option <tt>scanner-buttons-lock</tt> is set to <tt>SANE_TRUE</tt>
  </ul>

<p><li>
The frontend does read the value of option <tt>scanner-buttons-lock</tt>. If
it is <tt>SANE_TRUE</tt> then the frontend has access to the scanner buttons.
If it is <tt>SANE_FALSE</tt> then access has been denied.

<p><li>
If the button access is allowed the frontend has to do the following about
once per second (while not scanning):
  <ul>
  <li>
  The frontend does set option <tt>scanner-buttons-status-update</tt>.
  The backend checks if access to the buttons is allowed by comparing
  the backend PID with the lockfile PID. If access is allowed it
  does read the button status from the scanner and stores it in
  the option <tt>scanner-buttons-status</tt>, each bit represents a button, a
  value of 0 means the button is not pressed, a value of 1 means that the button
  is pressed. When the scanner is busy the backend must not wait, it has to
  return immediately and the button state keeps unchanged.
  The backend has to implement a timeout function. When no button has been pressed
  within a predefined time (e.g. 15 minutes) then the access permission is lost.
  In this case the backend does set option <tt>scanner-buttons-lock</tt> to <tt>SANE_FALSE</tt>
  and does set <tt>SANE_INFO_RELOAD_OPTIONS</tt> to inform the frontend that it has
  lost permission to access the scanner-button functions.
  If access is not allowed it does set the <tt>scanner-buttons-status</tt> to 0.

<p>  <li>
  The frontend does read the value of option <tt>scanner-buttons-status</tt>
  </ul>

<p><li>
When the frontend does exit or it does not want to use the buttons
it does set option <tt>scanner-buttons-lock</tt> to <tt>SANE_FALSE</tt>.
The backend does check if the backend PID and the lockfile PID are
the same. If this is true then it removes the lockfile and sets the value 
of <tt>scanner-buttons-lock</tt> to <tt>SANE_FALSE</tt>.

<p><tt>sane_close()</tt> should do the same as setting option
<tt>scanner-buttons-lock</tt> to <tt>SANE_FALSE</tt>.

<p></ul>
</font>

<p><font color="darkgreen">
<h3><a name="s4.5.15">4.5.15 Batch Scan Options</a></h3><a name="i139">

<p>The batch scan options can be used by a frontend to indicate that more than
one image will be scanned in a batch. The backend can optimize for such scans
by e.g. avoiding calibration and not moving home the sensor array in this case.
The backend provides the options <tt>batch-scan</tt>,
<tt>batch-scan-next-tl-x</tt>, <tt>batch-scan-next-tl-y</tt>,
<tt>batch-scan-next-br-x</tt>, and <tt>batch-scan-next-br-y</tt>. The option
<tt>batch-scan</tt> provides a string list with the values of <tt>No</tt> if
batch scanning isn't used, <tt>Start</tt> for the first, <tt>End</tt> for the last
and <tt>Loop</tt> for any other (intermediate) image.  The batch-scan-next
options specify the coordinates of the next scan and follow the same rules as
the scan area options (see section <a href="doc014.html#s4.5.4">4.5.4</a>).

<p>These options are not mandatory, but if a backend does support them, it must
implement them in a manner consistent with the above definition.  In this
case, all options must be implemented.
</font>

<p><p><hr>
<a href="doc015.html"><img src=../icons/next.gif alt="Next"></a>
<a href="doc000.html"><img src=../icons/up.gif alt="Up"></a>
<a href="doc013.html"><img src=../icons/previous.gif alt="Previous"></a>
<a href="doc000.html"><img src=../icons/contents.gif alt="Contents"></a>
<a href="doc019.html"><img src=../icons/index.gif alt="Index"></a>
<hr>
</body></html>
