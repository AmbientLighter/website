<html><body>
<a href="doc012.html"><img src=../icons/next.gif alt="Next"></a>
<a href="doc000.html"><img src=../icons/up.gif alt="Up"></a>
<a href="doc010.html"><img src=../icons/previous.gif alt="Previous"></a>
<a href="doc000.html"><img src=../icons/contents.gif alt="Contents"></a>
<a href="doc019.html"><img src=../icons/index.gif alt="Index"></a>
<hr>

<title>Data Types</title>
<h2><a name="s4.2">4.2 Data Types</a></h2>

<p><h3><a name="s4.2.1">4.2.1 Base Types</a></h3>

<p>The SANE standard is based on just two SANE-specific base types: the
SANE byte and word.
<blockquote>
  <tt>typedef <i>some-scalar-type</i> SANE_Byte<a name="i14">;</tt> <br>
  <tt>typedef <i>some-scalar-type</i> SANE_Word<a name="i15">;</tt>
</blockquote>
<tt>SANE_Byte</tt> must correspond to some scalar C type that is capable
of holding values in the range 0 to 255.  <tt>SANE_Word</tt> must be
capable of holding any of the following:
<ul>
  <li>the truth values <tt>SANE_FALSE</tt> and <tt>SANE_TRUE</tt>
  <li>signed integers in the range -2<sup>31</sup>...2<sup>31</sup>-1
  <li>fixed point values in the range -32768...32767.9999 with
    a resolution of 1/65536
  <li>32 bits (for bit sets)
</ul>
Note that the SANE standard does not define what C type
<tt>SANE_Byte</tt> and <tt>SANE_Word</tt> map to.  For example, on some
platforms, the latter may map to <tt>long int</tt> whereas on others it
may map to <tt>int</tt>.  A portable SANE frontend or backend must
therefore not depend on a particular mapping.

<p><h3><a name="s4.2.2">4.2.2 Boolean Type</a></h3>

<p><tt>SANE_Bool<a name="i16"></tt> is used for variables that can take one of
the two truth values <tt>SANE_FALSE<a name="i17"></tt> and
<tt>SANE_TRUE<a name="i18"></tt>.  The former value is defined to be 0,
whereas the latter is 1.<a name="F1"><a href="footnotes.html#000001"><sup><fontsize=-2>1</font></sup></a></a> The C
declarations for this type are given below.
<blockquote>
<pre>
#define SANE_FALSE      0
#define SANE_TRUE       1
typedef SANE_Word SANE_Bool;
</pre>
</blockquote>
Note that <tt>SANE_Bool</tt> is simply an alias of <tt>SANE_Word</tt>.  It
is therefore always legal to use the latter type in place of the
former.  However, for clarity, it is recommended to use
<tt>SANE_Bool</tt> whenever a given variable or formal argument has a
fixed interpretation as a boolean object.

<p><h3><a name="s4.2.3">4.2.3 Integer Type</a></h3>

<p><tt>SANE_Int<a name="i19"></tt> is used for variables that can take integer
values in the range -2<sup>32</sup> to 2<sup>31</sup>-1.  Its C declaration is
given below.
<blockquote>
<pre>
typedef SANE_Word SANE_Int;
</pre>
</blockquote>
Note that <tt>SANE_Int</tt> is simply an alias of <tt>SANE_Word</tt>.  It
is therefore always legal to use the latter type in place of the
former.  However, for clarity, it is recommended to use
<tt>SANE_Int</tt> whenever a given variable or formal argument has a
fixed interpretation as an integer object.

<p><h3><a name="s4.2.4">4.2.4 Fixed-point Type</a></h3>

<p><tt>SANE_Fixed<a name="i20"></tt> is used for variables that can take fixed
point values in the range -32768 to 32767.9999 with a resolution
of 1/65535.  The C declarations relating to this type are given
below.
<blockquote>
<pre>
#define SANE_FIXED_SCALE_SHIFT  16
typedef SANE_Word SANE_Fixed;
</pre>
</blockquote>
The macro <tt>SANE_FIXED_SCALE_SHIFT<a name="i21"></tt> gives the location
of the fixed binary point.  This standard defines that value to be 16,
which yields a resolution of 1/65536.

<p>Note that <tt>SANE_Fixed</tt> is simply an alias of <tt>SANE_Word</tt>.
It is therefore always legal to use the latter type in place of the
former.  However, for clarity, it is recommended to use
<tt>SANE_Fixed</tt> whenever a given variable or formal argument has a
fixed interpretation as a fixed-point object.

<p>For convenience, SANE also defines two macros that convert fixed-point
values to and from C double floating point values.
<blockquote>
  <dl>

<p>  <dt><tt>SANE_FIX<a name="i22">(<i>d</i>)</tt>:<dd> Returns the largest SANE
    fixed-point value that is smaller than the double value <i>d</i>.
    No range checking is performed.  If the value of <i>d</i> is out of
    range, the result is undefined.

<p>  <dt><tt>SANE_UNFIX<a name="i23">(<i>w</i>)</tt>:<dd> Returns the nearest
    double machine number that corresponds to fixed-point value
    <i>w</i>.

<p>  </dl>
</blockquote>
SANE does <em>not</em> require that the following two expressions hold
true (even if the values of <i>w</i> and <i>d</i> are in range):
<blockquote>
<pre>
SANE_UNFIX(SANE_FIX(d)) == d
SANE_FIX(SANE_UNFIX(w)) == w
</pre>
</blockquote>
In other words, conversion between fixed and double values may be
lossy.  It is therefore recommended to avoid repeated conversions
between the two representations.

<p><h3><a name="s4.2.5">4.2.5 Text</a></h3>

<p><h4><a name="s4.2.5.1">4.2.5.1 Character Type</a></h4>

<p>Type <tt>SANE_Char<a name="i24"></tt> represents a single text character or
symbol.  At present, this type maps directly to the underlying C
<tt>char</tt> type (typically one byte).  The encoding for such
characters is currently fixed as ISO LATIN-1.  Future versions of this
standard may map this type to a wider type and allow multi-byte
encodings to support internationalization.  As a result of this, care
should be taken to avoid the assumption that
<tt>sizeof(SANE_Char)==sizeof(char)</tt>.
<blockquote>
<pre>
typedef char SANE_Char;
</pre>
</blockquote>

<p><h4><a name="s4.2.5.2">4.2.5.2 String Type</a></h4>

<p>Type <tt>SANE_String<a name="i25"></tt> represents a text string as a sequence
of C <tt>char</tt> values.  The end of the sequence is indicated by a
<tt>'\0'</tt> (NUL<a name="i26">) character.  
<blockquote>
<pre>
typedef SANE_Char *SANE_String;
typedef const SANE_Char *SANE_String_Const;
</pre>
</blockquote>
The type <tt>SANE_String_Const<a name="i27"></tt> is provided by SANE to
enable declaring strings whose contents is unchangable.  Note that in
ANSI C, the declaration
<blockquote>
<pre>
const SANE_String str;
</pre>
</blockquote>
declares a string pointer that is constant (not a string pointer that
points to a constant value).

<p><h3><a name="s4.2.6">4.2.6 Scanner Handle Type</a></h3>

<p>Access to a scanner is provided through an opaque type called
<tt>SANE_Handle<a name="i28"></tt>.  The C declaration of this type is given
below.
<blockquote>
<pre>
typedef void *SANE_Handle;
</pre>
</blockquote>
While this type is declared to be a void pointer, an application must
not attempt to interpret the value of a <tt>SANE_Handle</tt>.  In
particular, SANE does not require that a value of this type is a legal
pointer value.

<p><h3><a name="s4.2.7">4.2.7 Status Type</a></h3>

<p>Most SANE operations return a value of type <tt>SANE_Status<a name="i29"></tt>
to indicate whether the completion status of the operation.  If an
operation completes successfully, <tt>SANE_STATUS_GOOD</tt> is returned.
In case of an error, a value is returned that indicates the nature of
the problem.  The complete list of available status codes is listed in
Table <a href="doc011.html#t1">1</a>.  It is recommended to use function
<tt>sane_strstatus()</tt> to convert status codes into a legible
string.

<p><p><a name="t1"></a>
  <center>
    <table cellpadding=0 cellspacing=0 border=1>
<tr valign=top>
<td colspan=1 align=center nowrap><b>Symbol</b></td>
<td colspan=1 align=center nowrap><b>Code</b></td>
<td colspan=1 align=center nowrap><b>Description</b></td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      
<tt>SANE_STATUS_GOOD<a name="i30"></tt>
        </td>
<td colspan=1 align=right nowrap>  0 </td>
<td colspan=1 align=left nowrap> Operation completed succesfully. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_STATUS_UNSUPPORTED<a name="i31"></tt>
        </td>
<td colspan=1 align=right nowrap>  1 </td>
<td colspan=1 align=left nowrap> Operation is not supported. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_STATUS_CANCELLED<a name="i32"></tt>
        </td>
<td colspan=1 align=right nowrap>  2 </td>
<td colspan=1 align=left nowrap> Operation was cancelled. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_STATUS_DEVICE_BUSY<a name="i33"></tt>
        </td>
<td colspan=1 align=right nowrap>  3 </td>
<td colspan=1 align=left nowrap> Device is busy---retry later. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_STATUS_INVAL<a name="i34"></tt>
        </td>
<td colspan=1 align=right nowrap>  4 </td>
<td colspan=1 align=left nowrap> Data or argument is invalid. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_STATUS_EOF<a name="i35"></tt>
        </td>
<td colspan=1 align=right nowrap>  5 </td>
<td colspan=1 align=left nowrap> No more data available (end-of-file). </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_STATUS_JAMMED<a name="i36"></tt>
        </td>
<td colspan=1 align=right nowrap>  6 </td>
<td colspan=1 align=left nowrap> Document feeder jammed. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_STATUS_NO_DOCS<a name="i37"></tt>
        </td>
<td colspan=1 align=right nowrap>  7 </td>
<td colspan=1 align=left nowrap> Document feeder out of documents. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_STATUS_COVER_OPEN<a name="i38"></tt>
        </td>
<td colspan=1 align=right nowrap>  8 </td>
<td colspan=1 align=left nowrap> Scanner cover is open. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_STATUS_IO_ERROR<a name="i39"></tt>
        </td>
<td colspan=1 align=right nowrap>  9 </td>
<td colspan=1 align=left nowrap> Error during device I/O. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_STATUS_NO_MEM<a name="i40"></tt>
        </td>
<td colspan=1 align=right nowrap> 10 </td>
<td colspan=1 align=left nowrap> Out of memory. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_STATUS_ACCESS_DENIED<a name="i41"></tt>
        </td>
<td colspan=1 align=right nowrap> 11 </td>
<td colspan=1 align=left nowrap> Access to resource has been denied. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      
    </td></tr>
</table>
    <p><center>Table 1: Status Codes</center>
  </center>
<p>

<p><h3><a name="s4.2.8">4.2.8 Device Descriptor Type</a></h3>
<font color="darkgreen">

<p>Each SANE device is represented by a structure of type
<tt>SANE_Device<a name="i42"></tt>.  The C declaration of this type is given
below.
<blockquote>
<pre>
typedef struct
  {
    SANE_String_Const name;
    SANE_String_Const vendor;
    SANE_String_Const model;
    SANE_String_Const type;
    SANE_String_Const email_backend_author;
    SANE_String_Const backend_website;
    SANE_String_Const device_location;
    SANE_String_Const comment;
    SANE_String_Const reserved_string;
    SANE_Int backend_version_code;
    SANE_Int backend_capablity_flags;
    SANE_Int reserved_int;
  }
SANE_Device;
</pre>
</blockquote>
</font>
<a name="i43">
The structure provides the unique name of the scanner in member
<tt>name</tt>.  It is this unique name that should be passed in a call
to <tt>sane_open()</tt>.  The format of this name is completely up to
the backend.  The only constraints are that the name is unique among
all devices supported by the backend and that the name is a legal SANE
text string.  To simplify presentation of unique names, their length
should not be excessive.  It is <em>recommended</em> that backends keep
unique names below 32 characters in length.  However, applications
<em>must</em> be able to cope with arbitrary length unique names.

<p>The next three members in the device structure provide additional
information on the device corresponding to the unique name.
Specifically, members <tt>vendor</tt>, <tt>model</tt>, and <tt>type</tt> are
single-line strings that give information on the vendor
(manufacturer), model, and the type of the device.  For consistency's
sake, the following strings should be used when appropriate (the lists
will be expanded as need arises):

<p><p><a name="t2"></a>
  <center>
    
    <spacer type=horizontal size=72>
    <table cellpadding=0 cellspacing=0 border=1>
<tr valign=top>
<td colspan=2 align=center nowrap><b>Vendor Strings<a name="i44"></b></td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      
      <tt>AGFA</tt>            </td>
<td colspan=1 align=left nowrap> <tt>Microtek</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>Abaton</tt>          </td>
<td colspan=1 align=left nowrap> <tt>Minolta</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>Acer</tt>            </td>
<td colspan=1 align=left nowrap> <tt>Mustek</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>Apple</tt>           </td>
<td colspan=1 align=left nowrap> <tt>NEC</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>Artec</tt>           </td>
<td colspan=1 align=left nowrap> <tt>Nikon</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>Avision</tt>         </td>
<td colspan=1 align=left nowrap> <tt>Plustek</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>CANON</tt>           </td>
<td colspan=1 align=left nowrap> <tt>Polaroid</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>Connectix</tt>       </td>
<td colspan=1 align=left nowrap> <tt>Ricoh</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>Epson</tt>           </td>
<td colspan=1 align=left nowrap> <tt>Sharp</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>Fujitsu</tt>         </td>
<td colspan=1 align=left nowrap> <tt>Siemens</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>Hewlett-Packard</tt> </td>
<td colspan=1 align=left nowrap> <tt>Tamarack</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>IBM</tt>             </td>
<td colspan=1 align=left nowrap> <tt>UMAX</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>Kodak</tt>           </td>
<td colspan=1 align=left nowrap> <tt>Noname</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>Logitech</tt>        </td>
<td colspan=1 align=left nowrap> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap> 
      
    </td></tr>
</table>
    <spacer type=horizontal size=72>
    <table cellpadding=0 cellspacing=0 border=1>
<tr valign=top>
<td colspan=1 align=center nowrap><b>Type Strings<a name="i45"></b></td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      
      <tt>film scanner</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>flatbed scanner</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>frame grabber</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>handheld scanner</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>multi-function peripheral</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>sheetfed scanner</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>still camera</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>video camera</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      <tt>virtual device</tt> </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
      
    </td></tr>
</table>
    <spacer type=horizontal size=72>
    <p><center>Table 2: Predefined Device Information Strings</center>
    
  </center>
<p>
Note that vendor string <tt>Noname</tt> can be used for virtual devices
that have no physical vendor associated.  Also, there are no
predefined model name strings since those are vendor specific and
therefore completely under control of the respective backends.

<p><font color="darkgreen">
The backend has to set up the string <tt>email_backend_author</tt>
with the name and the email address of the backend author or a contact person
in the format:
<pre>
Firstname Lastname &lt;name@domain.org&gt;
</pre>

<p>The string <tt>backend_website</tt> should be set up by the backend
with the website or ftp address of the backend in the format:
<pre>
http://www.domain.org/sane-hello/index.html
</pre>

<p>The backend should fill the string <tt>device_location</tt> with a
text that describes where a user can find this device. The text 
should be configurable by the administrator. This could e.g. look
like this:
<pre>
building 93, 2nd plane, room 2124
</pre>

<p>The string <tt>comment</tt> can be used to display any comment about the device
to the user. The text should be configurable by the administrator.

<p>The string <tt>reserved_string</tt> is unused currently but planned for
future use.

<p>All unused strings must be set to ``'' by the backend.

<p>With member <tt>backend_version_code</tt> a frontend can find
out the version of a backend it is connected to via one or more meta
backends.

<p>The member <tt>backend_capability_flags</tt> contains 32 bits that
are planned to give the backend the chance to inform the frontend about
its capabilities. The meaning of the flags will be defined when there
is the need for it. The backend has to set all not defined bits
(in the moment all 32 bits) to 0.

<p>The member <tt>reserved_int</tt> is planned for future use, the backend has
to set the value of the integer to 0.
</font>

<p><h3><a name="s4.2.9">4.2.9 Option Descriptor Type</a></h3>

<p>Option descriptors are at the same time the most intricate and
powerful type in the SANE standard.  Options are used to control
virtually all aspects of device operation.  Much of the power of the
SANE API stems from the fact that most device controls are completely
described by their respective option descriptor.  Thus, a frontend can
control a scanner abstractly, without requiring knowledge as to what
the purpose of any given option is.  Conversely, a scanner can
describe its controls without requiring knowledge of how the frontend
operates.  The C declaration of the
<tt>SANE_Option_Descriptor<a name="i46"></tt> type is given below.
<blockquote>
<pre>
typedef struct
  {
    SANE_String_Const name;
    SANE_String_Const title;
    SANE_String_Const desc;
    SANE_Value_Type type;
    SANE_Unit unit;
    SANE_Int size;
    SANE_Int cap;
    SANE_Constraint_Type constraint_type;
    union
      {
        const SANE_String_Const *string_list;
        const SANE_Word *word_list;
        const SANE_Range *range;
      }
    constraint;
  }
SANE_Option_Descriptor;
</pre>
</blockquote>

<p><h4><a name="s4.2.9.1">4.2.9.1 Option Name</a></h4>

<p>Member <tt>name</tt> is a string that uniquely identifies the option.
The name must be unique for a given device (i.e., the option names
across different backends or devices need not be unique).  The option
name must consist of lower-case ASCII letters (<tt>a</tt>--<tt>z</tt>),
digits (<tt>0</tt>--<tt>9</tt>), or the dash character (<tt>-</tt>) only.
The first character must be a lower-case ASCII character (i.e., not a
digit or a dash).

<p><h4><a name="s4.2.9.2">4.2.9.2 Option Title</a></h4>

<p>Member <tt>title</tt> is a single-line string that can be used by the
frontend as a title string.  This should typically be a short (one or
two-word) string that is chosen based on the function of the option.

<p><h4><a name="s4.2.9.3">4.2.9.3 Option Description</a></h4>

<p>Member <tt>desc</tt> is a (potentially very) long string that can be
used as a help text to describe the option.  It is the responsibility
of the frontend to break the string into managable-length lines.
Newline characters in this string should be interpreted as paragraph
breaks.

<p><h4><a name="s4.2.9.4">4.2.9.4 Option Value Type</a></h4>

<p>Member <tt>type</tt> specifies the type of the option value.  The
possible values for type <tt>SANE_Value_Type<a name="i47"></tt> are described
in Table <a href="doc011.html#t3">3</a>.

<p><p><a name="t3"></a>
  <center>
    
    <table cellpadding=0 cellspacing=0 border=1>
<tr valign=top>
<td colspan=1 align=center nowrap><b>Symbol</b></td>
<td colspan=1 align=center nowrap><b>Code</b></td>
<td colspan=1 align=center nowrap><b>Description</b></td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>


<p><tt>SANE_TYPE_BOOL<a name="i48"></tt> </td>
<td colspan=1 align=left nowrap> 0 </td>
<td colspan=1 align=left> Option value is of type
  <tt>SANE_Bool</tt>. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap> 

<p><tt>SANE_TYPE_INT<a name="i49"></tt> </td>
<td colspan=1 align=left nowrap> 1 </td>
<td colspan=1 align=left> Option value is of type
  <tt>SANE_Int</tt>. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap> 

<p><tt>SANE_TYPE_FIXED<a name="i50"></tt></td>
<td colspan=1 align=left nowrap>2 </td>
<td colspan=1 align=left> Option value is of type
  <tt>SANE_Fixed</tt>. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap> 

<p><tt>SANE_TYPE_STRING<a name="i51"></tt></td>
<td colspan=1 align=left nowrap>3 </td>
<td colspan=1 align=left> Option value is of type
  <tt>SANE_String</tt>. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap> 

<p><tt>SANE_TYPE_BUTTON<a name="i52"></tt> </td>
<td colspan=1 align=left nowrap> 4 </td>
<td colspan=1 align=left> An option of this type has no value.
Instead, setting an option of this type has an option-specific
side-effect.  For example, a button-typed option could be used by a
backend to provide a means to select default values or to the tell an
automatic document feeder to advance to the next sheet of paper. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap> 

<p><tt>SANE_TYPE_GROUP<a name="i53"></tt> </td>
<td colspan=1 align=left nowrap> 5 </td>
<td colspan=1 align=left> An option of this type has no value.
This type is used to group logically related options.  A group option
is in effect up to the point where another group option is encountered
(or up to the end of the option list, if there are no other group
options).  For group options, only members <tt>title</tt> and
<tt>type</tt> are valid in the option descriptor. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap> 

<p>  
    </td></tr>
</table>
    <p><center>Table 3: Option Value Types (<tt>SANE_Value_Type</tt>)</center>
    
  </center>
<p>

<p><h4><a name="s4.2.9.5">4.2.9.5 Option Value Unit</a></h4>

<p>Member <tt>unit</tt> specifies what the physical unit of the option
value is.  The possible values for type <tt>SANE_Unit<a name="i54"></tt> are
described in Table <a href="doc011.html#t4">4</a>.  Note that the specified unit is
what the SANE backend expects.  It is entirely up to a frontend as to
how these units a presented to the user.  For example, SANE expresses
all lengths in millimeters.  A frontend is generally expected to
provide appropriate conversion routines so that a user can express
quantities in a customary unit (e.g., inches or centimeters).

<p><p><a name="t4"></a>
  <center>
    
    <table cellpadding=0 cellspacing=0 border=1>
<tr valign=top>
<td colspan=1 align=center nowrap><b>Symbol</b></td>
<td colspan=1 align=center nowrap><b>Code</b></td>
<td colspan=1 align=center nowrap><b>Description</b></td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>

<p>

<p><tt>SANE_UNIT_NONE<a name="i55"></tt>   </td>
<td colspan=1 align=left nowrap> 0 </td>
<td colspan=1 align=left nowrap> Value is unit-less (e.g., page count).</td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_UNIT_PIXEL<a name="i56"></tt>  </td>
<td colspan=1 align=left nowrap> 1 </td>
<td colspan=1 align=left nowrap> Value is in number of pixels. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_UNIT_BIT<a name="i57"></tt>    </td>
<td colspan=1 align=left nowrap> 2 </td>
<td colspan=1 align=left nowrap> Value is in number of bits. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_UNIT_MM<a name="i58"></tt>     </td>
<td colspan=1 align=left nowrap> 3 </td>
<td colspan=1 align=left nowrap> Value is in millimeters. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_UNIT_DPI<a name="i59"></tt>    </td>
<td colspan=1 align=left nowrap> 4 </td>
<td colspan=1 align=left nowrap> Value is a resolution in dots/inch. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_UNIT_PERCENT<a name="i60"></tt></td>
<td colspan=1 align=left nowrap> 5 </td>
<td colspan=1 align=left nowrap> Value is a percentage. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_UNIT_MICROSECOND<a name="i61"></tt></td>
<td colspan=1 align=left nowrap> 6 </td>
<td colspan=1 align=left nowrap> Value is time in &micro;-seconds. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>

<p>
    </td></tr>
</table>
    <p><center>Table 4: Physical Units (<tt>SANE_Unit</tt>)</center>
    
  </center>
<p>

<p><h4><a name="s4.2.9.6">4.2.9.6 Option Value Size</a></h4>

<p>Member <tt>size</tt> specifies the size of the option value (in bytes).
This member has a slightly different interpretation depending on the
type of the option value:
<blockquote>
  <dl>
  <dt><tt>SANE_TYPE_STRING</tt>:<dd> The size is the maximum size of
    the string.  For the purpose of string size calcuations, the
    terminating <tt>NUL</tt> character is considered to be part of the
    string.  Note that the terminating <tt>NUL</tt> character must
    always be present in string option values.
  <dt><tt>SANE_TYPE_INT</tt>, <tt>SANE_TYPE_FIXED</tt>:<dd> The size
    must be a positive integer multiple of the size of a
    <tt>SANE_Word</tt>.  The option value is a vector of length
    
<p>
<center>
 <tt>size</tt>/<tt>sizeof(SANE_Word)</tt>. </center>
<p>
  <dt><tt>SANE_TYPE_BOOL</tt>:<dd> The size must be set to
    <tt>sizeof(SANE_Word)</tt>.
  <dt><tt>SANE_TYPE_BUTTON</tt>, <tt>SANE_TYPE_GROUP</tt>:<dd> The
    option size is ignored.
  </dl>
</blockquote>

<p><h4><a name="s4.2.9.7">4.2.9.7 Option Capabilities</a></h4>

<p>Member <tt>cap</tt> describes what capabilities the option posseses.
This is a bitset that is formed as the inclusive logical OR of the
capabilities described in Table <a href="doc011.html#t5">5</a>.  The SANE API
provides the following to macros to test certain features of a given
capability bitset:
<blockquote>
  <dl>

<p>  <dt><tt>SANE_OPTION_IS_ACTIVE<a name="i62">(<i>cap</i>)</tt>:<dd> This macro
    returns <tt>SANE_TRUE</tt> if and only if the option with the
    capability set <i>cap</i> is currently active.

<p>  <dt><tt>SANE_OPTION_IS_SETTABLE<a name="i63">(<i>cap</i>)</tt>:<dd> This
    macro returns <tt>SANE_TRUE</tt> if and only if the option with the
    capability set <i>cap</i> is software settable.
  </dl>
</blockquote>

<p><p><a name="t5"></a>
  <center>
    
    <table cellpadding=0 cellspacing=0 border=1>
<tr valign=top>
<td colspan=1 align=center nowrap><b>Symbol</b></td>
<td colspan=1 align=center nowrap><b>Code</b></td>
<td colspan=1 align=center nowrap><b>Description</b></td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>


<p><tt>SANE_CAP_SOFT_SELECT<a name="i64"></tt> </td>
<td colspan=1 align=right nowrap> 1 </td>
<td colspan=1 align=left> The option
  value can be set by a call to <tt>sane_control_option()</tt>.</td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap> 

<p><tt>SANE_CAP_HARD_SELECT<a name="i65"></tt> </td>
<td colspan=1 align=right nowrap> 2 </td>
<td colspan=1 align=left> The option value can be set by
  user-intervention (e.g., by flipping a switch).  The user-interface
  should prompt the user to execute the appropriate action to set such
  an option.  This capability is mutually exclusive with
  SANE_CAP_SOFT_SELECT (either one of them can be set, but not both
  simultaneously).  </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap> 

<p><tt>SANE_CAP_SOFT_DETECT<a name="i66"></tt> </td>
<td colspan=1 align=right nowrap> 4 </td>
<td colspan=1 align=left> The option
  value can be detected by software.  If
  <tt>SANE_CAP_SOFT_SELECT</tt> is set, this capability <em>must</em>
  be set.  If <tt>SANE_CAP_HARD_SELECT</tt> is set, this capability
  may or may not be set.  If this capability is set but neither
  <tt>SANE_CAP_SOFT_SELECT</tt> nor <tt>SANE_CAP_HARD_SELECT</tt>
  are, then there is no way to control the option.  That is, the
  option provides read-out of the current value only.  </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap> 

<p><tt>SANE_CAP_EMULATED<a name="i67"></tt> </td>
<td colspan=1 align=right nowrap> 8 </td>
<td colspan=1 align=left> If set, this capability indicates
  that an option is not directly supported by the device and is
  instead emulated in the backend.  A sophisticated frontend may
  elect to use its own (presumably better) emulation in lieu of an emulated
  option. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap> 

<p><tt>SANE_CAP_AUTOMATIC<a name="i68"></tt> </td>
<td colspan=1 align=right nowrap> 16 </td>
<td colspan=1 align=left> If set, this capability indicates
  that the backend (or the device) is capable to picking a reasonable
  option value automatically.  For such options, it is possible to
  select automatic operation by calling <tt>sane_control_option()</tt>
  with an action value of <tt>SANE_ACTION_SET_AUTO</tt>.  </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap> 

<p><tt>SANE_CAP_INACTIVE<a name="i69"></tt> </td>
<td colspan=1 align=right nowrap> 32 </td>
<td colspan=1 align=left> If set, this capability indicates
  that the option is not currently active (e.g., because it's
  meaningful only if another option is set to some other value). </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap> 

<p><tt>SANE_CAP_ADVANCED<a name="i70"></tt> </td>
<td colspan=1 align=right nowrap> 64 </td>
<td colspan=1 align=left>
  If set, this capability indicates that the option should be
  considered an ``advanced user option''. 
<font color="darkgreen">
  If this capability is set for an option of type
  <tt>SANE_TYPE_GROUP</tt>, all options belonging to the group are also
  advanced, even if they don't set the capabilty themselves. 
</font>
  A frontend typically
  displays such options in a less conspicuous way than regular options
  (e.g., a command line interface may list such options last or a
  graphical interface may make them available in a seperate ``advanced
  settings'' dialog). 
  </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>

<p><tt>SANE_CAP_HIDDEN<a name="i71"></tt> </td>
<td colspan=1 align=right nowrap> 128 </td>
<td colspan=1 align=left>
<font color="darkgreen">
  If set, this capability indicates that the option shouldn't be
  displayed to and used by the user directly. Instead a hidden option
  is supposed to be automatically used by the frontend, like e.g. the
  ``preview'' option.
  If this capability is set for an option of type
  <tt>SANE_TYPE_GROUP</tt>, all options belonging to the group are also
  hidden, even if they don't set the capabilty themselves. 
  A frontend typically doesn't display such options by default but there
  should be a way to override this default behaviour.
</font>
  </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>

<p><tt>SANE_CAP_ALWAYS_SETTABLE<a name="i72"></tt> </td>
<td colspan=1 align=right nowrap> 256 </td>
<td colspan=1 align=left> 
<font color="darkgreen">
  If set, this capability indicates that the option may be at any time between
  <tt>sane_open()</tt> and <tt>sane_close()</tt>. I.e.  it's allowed to set it
  even while an image is acquired.
</font>
</td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>

<p>
    </td></tr>
</table>
    <p><center>Table 5: Option Capabilities</center>
    
  </center>
<p>

<p><h4><a name="s4.2.9.8">4.2.9.8 Option Value Constraints</a></h4>

<p>It is often useful to constrain the values that an option can take.
For example, constraints can be used by a frontend to determine how to
represent a given option.  Member <tt>constraint_type</tt> indicates
what constraint is in effect for the option.  The constrained values
that are allowed for the option are described by one of the union
members of member <tt>constraint</tt>.  The possible values of type
<tt>SANE_Constraint_Type<a name="i73"></tt> and the interpretation of the
<tt>constraint</tt> union is described in Table <a href="doc011.html#t6">6</a>.

<p><p><a name="t6"></a>
  <center>
    
    <table cellpadding=0 cellspacing=0 border=1>
<tr valign=top>
<td colspan=1 align=center nowrap><b>Symbol</b></td>
<td colspan=1 align=center nowrap><b>Code</b></td>
<td colspan=1 align=center nowrap><b>Description</b></td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>

<p>

<p><tt>SANE_CONSTRAINT_NONE<a name="i74"></tt> </td>
<td colspan=1 align=right nowrap> 0 </td>
<td colspan=1 align=left> The value is unconstrained.
  The option can take any of the values possible for the option's
  type. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>

<p>  <tt>SANE_CONSTRAINT_RANGE<a name="i75"></tt> </td>
<td colspan=1 align=right nowrap> 1 </td>
<td colspan=1 align=left> This constraint is
  applicable to integer and fixed-point valued options only.  It
  constrains the option value to a possibly quantized range of
  numbers.  Option descriptor member <tt>constraint.range</tt> points to
  a range of the type <tt>SANE_Range<a name="i76"></tt>.  This type is illustrated
  below:
  <blockquote>
<pre>
typedef struct
  {
    SANE_Word min;
    SANE_Word max;
    SANE_Word quant;
  }
SANE_Range;
</pre>
  </blockquote>
  All three members in this structure are interpreted according to the
  option value type (<tt>SANE_TYPE_INT</tt> or <tt>SANE_TYPE_FIXED</tt>).
  Members <tt>min</tt> and <tt>max</tt> specify the minimum and maximum
  values, respectively.  If member <tt>quant</tt> is non-zero, it
  specifies the quantization value.  If l is the minimum value, u
  the maximum value and q the (non-zero) quantization of a range,
  then the legal values are v=k*q+l for all non-negative
  integer values of k such that v&lt;=u.  </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap> 

<p><tt>SANE_CONSTRAINT_WORD_LIST<a name="i77"></tt> </td>
<td colspan=1 align=right nowrap> 2 </td>
<td colspan=1 align=left> This constraint is applicable
  to integer and fixed-point valued options only.  It constrains the
  option value to a list of numeric values.  Option descriptor member
  <tt>constraint.word_list</tt> points to a list of words that
  enumerates the legal values.  The first element in that list is an
  integer (<tt>SANE_Int</tt>) that specifies the length of the list (not
  counting the length itself).  The remaining elements in the list are
  interpreted according to the type of the option value
  (<tt>SANE_TYPE_INT</tt> or <tt>SANE_TYPE_FIXED</tt>). </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap> 

<p><tt>SANE_CONSTRAINT_STRING_LIST<a name="i78"></tt> </td>
<td colspan=1 align=right nowrap> 3 </td>
<td colspan=1 align=left> This constraint is
  applicable to string-valued options only.  It constrains the option
  value to a list of strings.  The option descriptor member
  <tt>constraint.string_list</tt> points to a <tt>NULL</tt> terminated
  list of strings that enumerate the legal values for the option
  value.
</td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
    </td></tr>
</table>
    <p><center>Table 6: Option Value Constraints</center>
    
  </center>
<p>

<p><h3><a name="s4.2.10">4.2.10 Internationalization</a></h3>
<font color="darkgreen">
All backend texts should be written in english.
Localization (translation of backend texts) has to be done
in the frontend. To automatically prepare translation tables
(e.g. english to german) it is necessary to mark the texts
that shall be translated.

<p><h4><a name="s4.2.10.1">4.2.10.1 How is a text marked for translation</a></h4>

<p>The keyword <tt>SANE_I18N</tt> is used to mark a text for translation.
<tt>SANE_I18N</tt> has to be defined as a dummy prototype:

<p>  <blockquote>
<pre>
#ifndef SANE_I18N
#  define SANE_I18N(text) text
#endif
</pre>
  </blockquote>

<p>You should not mark prototypes or variables with <tt>SANE_I18N</tt>
because it is not possible (or very hard) to find out the
texts that are covered by prototypes (<tt>SANE_I18N(START_SCAN_TEXT)</tt>) and
variables (<tt>SANE_I18N(option[7].name)</tt>).

<p>A correct marked text can look like this:<br>
<tt>snprintf(buf, sizeof(buf), SANE_I18N("Start scan")</tt><br>
or<br>
<tt>#define START_SCAN_TEXT SANE_I18N("Start scan")</tt>.<br>
It also is allowed to mark texts in structs because the
prototype <tt>SANE_I18N</tt> has no effect to the compiler.

<p><h4><a name="s4.2.10.2">4.2.10.2 Which texts shall be marked for translation?</a></h4>
All option texts that are visible for the user should be marked for
translation. This is:
<ul>
<li>
member <tt>title</tt>
<li>
member <tt>desc</tt>
<li>
member <tt>string_list</tt>
</ul>
of <tt>SANE_Option_Descriptor</tt>.

<p>It is not allowed to mark/translate member <tt>name</tt>.
Please also do not mark any error or debug messages that are
displayed by the backend.

<p><h4><a name="s4.2.10.3">4.2.10.3 File formats and translation functions</a></h4>
The recommended file formats for translation tables are the <tt>po</tt> files
and <tt>mo</tt> or <tt>gmo</tt> files.
The po file contains the original text marked with the keyword <tt>msgid</tt>
and the translated text marked with the keyword <tt>msgstr</tt>.
A <tt>po</tt> file that contains all needed <tt>msgid</tt>s can be created
e.g. by the gnu gettext tool <tt>xgettext</tt> with the option
<tt>-k SANE_I18N</tt>.
The translator adds the translated texts to the <tt>po</tt> files.
The gettext tool <tt>msgfmt</tt> converts the <tt>po</tt> files to the
<tt>mo</tt> or <tt>gmo</tt> files.<br>

<p>Translation is done in the frontend. A backend has nothing
to do with the translation of texts.
The frontend should use the function <tt>gettext</tt>
to translate the texts. This e.g. can look like this:<br>
<tt>snprintf(buf, sizeof(buf), gettext("english text"));</tt>

<p>If a frontend author decides to use translation functions that
need different translation tables, then the frontend is
responsible to create/convert the translation tables.
In this case it should use the <tt>po</tt> files to create its
own translation tables from it.<br>

<p>Note that it is strongly recommended to use the gettext tools.
</font>

<p><p><hr>
<a href="doc012.html"><img src=../icons/next.gif alt="Next"></a>
<a href="doc000.html"><img src=../icons/up.gif alt="Up"></a>
<a href="doc010.html"><img src=../icons/previous.gif alt="Previous"></a>
<a href="doc000.html"><img src=../icons/contents.gif alt="Contents"></a>
<a href="doc019.html"><img src=../icons/index.gif alt="Index"></a>
<hr>
</body></html>
