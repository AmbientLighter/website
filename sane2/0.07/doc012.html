<html><body>
<a href="doc013.html"><img src=../icons/next.gif alt="Next"></a>
<a href="doc000.html"><img src=../icons/up.gif alt="Up"></a>
<a href="doc011.html"><img src=../icons/previous.gif alt="Previous"></a>
<a href="doc000.html"><img src=../icons/contents.gif alt="Contents"></a>
<a href="doc019.html"><img src=../icons/index.gif alt="Index"></a>
<hr>

<title>Operations</title>
<h2><a name="s4.3">4.3 Operations</a></h2>

<p><h3><a name="s4.3.1">4.3.1 <tt>sane_init</tt></a></h3>

<p>This function must be called before any other SANE function can be called.
The behavior of a SANE backend is undefined if this function is not called
first or if the status code returned by <tt>sane_init</tt> is different from
<tt>SANE_STATUS_GOOD<a name="i77"></tt>.  The version code of the backend is returned
in the value pointed to by <tt>version_code</tt>.  If that pointer is
<tt>NULL</tt>, no version code is returned.  Argument <tt>authorize</tt> is either
a pointer to a function that is invoked when the backend requires
authentication for a specific resource or <tt>NULL</tt> if the frontend does not
support authentication.
<blockquote><a name="i78">
<pre>
SANE_Status sane_init (SANE_Int * version_code,
                       SANE_Authorization_Callback authorize);
</pre>
</blockquote>

<p>The authorization function may be called by a backend in response to
any of the following calls:
<blockquote>
  <tt>sane_open</tt>, <tt>sane_control_option</tt>, <tt>sane_start</tt>
</blockquote>
If a backend was initialized without authorization function, then
authorization requests that cannot be handled by the backend itself
will fail automatically and the user may be prevented from accessing
protected resources.  Backends are encouraged to implement means of
authentication that do not require user assistance.  E.g., on a
multi-user system that authenticates users through a login process a
backend could automatically lookup the apporpriate password based on
resource- and user-name.

<p>The authentication function type has the following declaration:
<blockquote><a name="i79">
  <a name="i80"><a name="i81"><a name="i82">
<pre>
#define SANE_MAX_USERNAME_LEN   128
#define SANE_MAX_PASSWORD_LEN   128

typedef void (*SANE_Authorization_Callback)
    (SANE_String_Const resource,
     SANE_Char username[SANE_MAX_USERNAME_LEN],
     SANE_Char password[SANE_MAX_PASSWORD_LEN]);
</pre>
</blockquote>
Three arguments are passed to the authorization function:
<tt>resource</tt> is a string specifying the name of the resource that
requires authorization.  A frontend should use this string to build a
user-prompt requesting a username and a password.  The <tt>username</tt>
and <tt>password</tt> arguments are (pointers to) an array of
<tt>SANE_MAX_USERNAME_LEN</tt> and <tt>SANE_MAX_PASSWORD_LEN</tt>
characters, respectively.  The authorization call should place the
entered username and password in these arrays.  The returned strings
<em>must</em> be ASCII-NUL terminated.

<p><h3><a name="s4.3.2">4.3.2 <tt>sane_exit</tt></a></h3>

<p>This function must be called to terminate use of a backend.  The
function will first close all device handles that still might be open
(it is recommended to close device handles explicitly through a call
to <tt>sane_close()</tt>, but backends are required to release all
resources upon a call to this function).  After this function returns,
no function other than <tt>sane_init()</tt> may be called (regardless
of the status value returned by <tt>sane_exit()</tt>.  Neglecting to
call this function may result in some resources not being released
properly.
<blockquote><a name="i83">
<pre>
void sane_exit (void);
</pre>
</blockquote>

<p><h3><a name="s4.3.3">4.3.3 <tt>sane_get_devices</tt></a></h3>

<p>This function can be used to query the list of devices that are
available.  If the function executes successfully, it stores a pointer
to a <tt>NULL</tt> terminated array of pointers to <tt>SANE_Device</tt>
structures in <tt>*device_list</tt>.  The returned list is guaranteed
to remain unchanged and valid until (a) another call to this function
is performed or (b) a call to <tt>sane_exit()</tt> is performed.  This
function can be called repeatedly to detect when new devices become
available.  If argument <tt>local_only</tt> is true, only local devices
are returned (devices directly attached to the machine that SANE is
running on).  If it is false, the device list includes all remote
devices that are accessible to the SANE library.
<blockquote><a name="i84">
<pre>
SANE_Status sane_get_devices (const SANE_Device *** device_list,
                              SANE_Bool local_only);
</pre>
</blockquote>

<p>This function may fail with <tt>SANE_STATUS_NO_MEM</tt> if an
insufficient amount of memory is available.

<p><blockquote>
  <center>
    <b>Backend Implementation Note</b>
  </center>
  SANE does not require that this function is called before a
  <tt>sane_open()</tt> call is performed.  A device name may be
  specified explicitly by a user which would make it unnecessary and
  undesirable to call this function first.
<font color="darkgreen">
  The same information about
  a device has to be returned when <tt>sane_open</tt> is called.
</font>
</blockquote>

<p><h3><a name="s4.3.4">4.3.4 <tt>sane_open</tt></a></h3>

<p>This function is used to establish a connection to a particular
device.  The name of the device to be opened is passed in argument
<tt>name</tt>.  If the call completes successfully, a handle for the
device is returned in <tt>*h</tt>.
<font color="darkgreen">
The description of the device
is returned in <tt>**device_description</tt>. This is the
same description that is returned in the list by <tt>sane_get_devices</tt>.
The returned data <tt>*h</tt> and <tt>*device_description</tt> is guaranteed
to remain unchanged and valid until a call to <tt>sane_close()</tt>
is performed.
</font>
As a special case, specifying a zero-length string as the device requests
opening the first available device (if there is such a device).
<blockquote><a name="i85">
<pre>
SANE_Status sane_open (SANE_String_Const name, SANE_Handle * h,
const SANE_Device ** device_description);
</pre>
</blockquote>

<p>This function may fail with one of the following status codes.
<blockquote>
<dl>
<dt><tt>SANE_STATUS_DEVICE_BUSY</tt>:<dd> The device is currently
  busy (in use by somebody else).
<dt><tt>SANE_STATUS_INVAL</tt>:<dd> The device name is not valid.
<dt><tt>SANE_STATUS_IO_ERROR</tt>:<dd> An error occured while
  communicating with the device.
<dt><tt>SANE_STATUS_NO_MEM</tt>:<dd> An insufficent amount of memory
  is available.
<dt><tt>SANE_STATUS_ACCESS_DENIED</tt>:<dd> Access to the device has
  been denied due to insufficient or invalid authentication.
</dl>
</blockquote>

<p><h3><a name="s4.3.5">4.3.5 <tt>sane_close</tt></a></h3>

<p>This function terminates the association between the device handle
passed in argument <tt>h</tt> and the device it represents.  If the
device is presently active, a call to <tt>sane_cancel()</tt> is
performed first.  After this function returns, handle <tt>h</tt> must
not be used anymore.

<p><blockquote><a name="i86">
<pre>
void sane_close (SANE_Handle h);
</pre>
</blockquote>

<p><h3><a name="s4.3.6">4.3.6 <tt>sane_get_option_descriptor</tt></a></h3>

<p>This function is used to access option descriptors.  The function
returns the option descriptor for option number <tt>n</tt> of the device
represented by handle <tt>h</tt>.  Option number 0 is guaranteed to be a
valid option.  Its value is an integer that specifies the number of
options that are available for device handle <tt>h</tt> (the count
includes option 0).  If n is not a valid option index, the function
returns <tt>NULL</tt>.  The returned option descriptor is guaranteed to
remain valid (and at the returned address) until the device is closed.

<p><blockquote><a name="i87">
<pre>
const SANE_Option_Descriptor *
    sane_get_option_descriptor (SANE_Handle h, SANE_Int n);
</pre>
</blockquote>

<p><h3><a name="s4.3.7">4.3.7 <tt>sane_control_option</tt></a></h3>

<p>This function is used to set or inquire the current value of option
number <tt>n</tt> of the device represented by handle <tt>h</tt>.  The
manner in which the option is controlled is specified by parameter
<tt>a</tt>.  The possible values of this parameter are described in more
detail below.  The value of the option is passed through argument
<tt>v</tt>.  It is a pointer to the memory that holds the option value.
The memory area pointed to by <tt>v</tt> must be big enough to hold the
entire option value (determined by member <tt>size</tt> in the
corresponding option descriptor).  The only exception to this rule is
that when setting the value of a string option, the string pointed to
by argument <tt>v</tt> may be shorter since the backend will stop
reading the option value upon encountering the first <tt>NUL</tt>
terminator in the string.  If argument <tt>i</tt> is not <tt>NULL</tt>,
the value of <tt>*i</tt> will be set to provide details on how well the
request has been met.  The meaning of this argument is described in
more detail below.
<blockquote><a name="i88">
<pre>
SANE_Status sane_control_option (SANE_Handle h, SANE_Int n,
                                 SANE_Action a, void *v,
                                 SANE_Int * i);
</pre>
</blockquote>

<p>The way the option is affected by a call to this function is
controlled by parameter <tt>a</tt> which is a value of type
<tt>SANE_Action<a name="i89"></tt>.  The possible values and their meaning is
described in Table <a href="doc012.html#t7">7</a>.

<p><p><a name="t7"></a>
  <center>
    
    <table cellpadding=0 cellspacing=0 border=1>
<tr valign=top>
<td colspan=1 align=center nowrap><b>Symbol</b></td>
<td colspan=1 align=center nowrap><b>Code</b></td>
<td colspan=1 align=center nowrap><b>Description</b></td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>

<p>

<p><tt>SANE_ACTION_GET_VALUE<a name="i90"></tt> </td>
<td colspan=1 align=right nowrap> 0 </td>
<td colspan=1 align=left> Get current option value. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>

<p><tt>SANE_ACTION_SET_VALUE<a name="i91"></tt> </td>
<td colspan=1 align=right nowrap> 1 </td>
<td colspan=1 align=left> Set option value.  The
  option value passed through argument <tt>v</tt> may be modified by the
  backend if the value cannot be set exactly.  </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap> 

<p><tt>SANE_ACTION_SET_AUTO<a name="i92"></tt> </td>
<td colspan=1 align=right nowrap> 2 </td>
<td colspan=1 align=left> Turn on automatic mode.  Backend
  or device will automatically select an appropriate value.  This mode
  remains effective until overridden by an explicit set value request.
  The value of parameter <tt>v</tt> is completely ignored in this case and
  may be <tt>NULL</tt>.  </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>

<p>
    </td></tr>
</table>
    <p><center>Table 7: Action Values (<tt>SANE_Action</tt>)</center>
    
  </center>
<p>

<p>After setting a value via an action value of
<tt>SANE_ACTION_SET_VALUE</tt>, additional information on how well the
request has been met is returned in <tt>*i</tt> (if <tt>i</tt> is
non-<tt>NULL</tt>).  The returned value is a bitset that may contain any
combination of the values described in Table <a href="doc012.html#t8">8</a>.
<p><a name="t8"></a>
  <center>
    
    <table cellpadding=0 cellspacing=0 border=1>
<tr valign=top>
<td colspan=1 align=center nowrap><b>Symbol</b></td>
<td colspan=1 align=center nowrap><b>Code</b></td>
<td colspan=1 align=center nowrap><b>Description</b></td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>

<p>

<p><tt>SANE_INFO_INEXACT<a name="i93"></tt> </td>
<td colspan=1 align=right nowrap> 1 </td>
<td colspan=1 align=left> This value is returned when
  setting an option value resulted in a value being selected that does
  not exactly match the requested value.  For example, if a scanner
  can adjust the resolution in increments of 30dpi only, setting the
  resolution to 307dpi may result in an actual setting of 300dpi.
  When this happens, the bitset returned in <tt>*i</tt> has this member
  set.  In addition, the option value is modified to reflect the
  actual (rounded) value that was used by the backend.  Note that
  inexact values are admissible for strings as well.  A backend may
  choose to ``round'' a string to the closest matching legal string
  for a constrained string value. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>

<p><tt>SANE_INFO_RELOAD_OPTIONS<a name="i94"></tt> </td>
<td colspan=1 align=right nowrap> 2 </td>
<td colspan=1 align=left> The setting of an
  option may affect the value or availability of one or more <em>
    other</em> options.  When this happens, the SANE backend sets this
  member in <tt>*i</tt> to indicate that the application should reload
  all options.  This member may be set if and only if at least one
  option changed. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>

<p><tt>SANE_INFO_RELOAD_PARAMS<a name="i95"></tt> </td>
<td colspan=1 align=right nowrap> 4 </td>
<td colspan=1 align=left> The setting of an option may
  affect the parameter values (see <tt>sane_get_parameters()</tt>).
  If setting an option affects the parameter values, this member will
  be set in <tt>*i</tt>.  Note that this member may be set even if the
  parameters did not actually change.  However, it is guaranteed that
  the parameters never change without this member being set. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>

<p>
    </td></tr>
</table>
    <p><center>Table 8: Additional Information Returned When Setting an Option</center>
    
  </center>
<p>

<p>This function may fail with one of the following status codes.
<blockquote>
<dl>
<dt><tt>SANE_STATUS_UNSUPPORTED</tt>:<dd> The operation is not
  supported for the specified handle and option number.
<dt><tt>SANE_STATUS_INVAL</tt>:<dd> The option value is not valid.
<dt><tt>SANE_STATUS_IO_ERROR</tt>:<dd> An error occured while
  communicating with the device.
<dt><tt>SANE_STATUS_NO_MEM</tt>:<dd> An insufficent amount of memory
  is available.
<dt><tt>SANE_STATUS_ACCESS_DENIED</tt>:<dd> Access to the option has
  been denied due to insufficient or invalid authentication.
</dl>
</blockquote>

<p><h3><a name="s4.3.8">4.3.8 <tt>sane_get_parameters</tt></a></h3>

<p>This function is used to obtain the current scan parameters.  The
returned parameters are guaranteed to be accurate between the time a
scan has been started (<tt>sane_start()</tt> has been called) and the
completion of that request.  Outside of that window, the returned
values are best-effort estimates of what the parameters will be when
<tt>sane_start()</tt> gets invoked.  Calling this function before a
scan has actually started allows, for example, to get an estimate of
how big the scanned image will be.  The parameters passed to this
function are the handle <tt>h</tt> of the device for which the
parameters should be obtained and a pointer <tt>p</tt> to a parameter
structure.  The parameter structure is described in more detail below.

<p><blockquote><a name="i96">
<pre>
SANE_Status sane_get_parameters (SANE_Handle h,
                                 SANE_Parameters * p);
</pre>
</blockquote>

<p>The scan parameters are returned in a structure of type
<tt>SANE_Parameters<a name="i97"></tt>.  The C declaration of this structure
is given below.
<font color="darkgreen">
<blockquote>
<pre>
typedef struct
  {
    SANE_Frame format;
    SANE_Int flags;
    SANE_Int lines;
    SANE_Int depth;
    SANE_Int pixels_per_line;
    SANE_Int bytes_per_line;
    SANE_Int channels_per_image;
    SANE_String format_desc;
    SANE_String proposed_filename;
    SANE_Int dpi_x;
    SANE_Int dpi_y;
    char reserved[32]; /* 32 bytes for future use */
  }
SANE_Parameters;
</pre>
</blockquote>
</font>

<p><font color="darkgreen">
Member <tt>format</tt> specifies the format of the next frame to be
returned.  The possible values for type <tt>SANE_Frame<a name="i98"></tt> are
described in Table <a href="doc012.html#t9">9</a>.  The meaning of these
values is described in more detail in Section <a href="doc008.html#s3.2">3.2</a>.
<p><a name="t9"></a>
  <center>
    
    <table cellpadding=0 cellspacing=0 border=1>
<tr valign=top>
<td colspan=1 align=center nowrap><b>Symbol</b></td>
<td colspan=1 align=center nowrap><b>Code</b></td>
<td colspan=1 align=center nowrap><b>SANE standard</b></td>
<td colspan=1 align=center nowrap><b>Description</b></td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>

<p>

<p><tt>SANE_FRAME_GRAY<a name="i99"></tt>  </td>
<td colspan=1 align=center nowrap> 0 </td>
<td colspan=1 align=center nowrap> version 1 </td>
<td colspan=1 align=left nowrap> Band covering human visual range. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_FRAME_RGB<a name="i100"></tt>   </td>
<td colspan=1 align=center nowrap> 1 </td>
<td colspan=1 align=center nowrap> version 1 </td>
<td colspan=1 align=left nowrap> Pixel-interleaved red/green/blue bands. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_FRAME_RED<a name="i101"></tt>   </td>
<td colspan=1 align=center nowrap> 2 </td>
<td colspan=1 align=center nowrap> version 1 </td>
<td colspan=1 align=left nowrap> Red band of a red/green/blue image. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_FRAME_GREEN<a name="i102"></tt> </td>
<td colspan=1 align=center nowrap> 3 </td>
<td colspan=1 align=center nowrap> version 1 </td>
<td colspan=1 align=left nowrap> Green band of a red/green/blue image. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_FRAME_BLUE<a name="i103"></tt>  </td>
<td colspan=1 align=center nowrap> 4 </td>
<td colspan=1 align=center nowrap> version 1 </td>
<td colspan=1 align=left nowrap> Blue band of a red/green/blue image. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_FRAME_RAW<a name="i104"></tt>   </td>
<td colspan=1 align=center nowrap> 5 </td>
<td colspan=1 align=center nowrap> version 2 </td>
<td colspan=1 align=left nowrap> Arbitrary pixel property transmission. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>
<tt>SANE_FRAME_MIME<a name="i105"></tt>  </td>
<td colspan=1 align=center nowrap> 6 </td>
<td colspan=1 align=center nowrap> version 2 </td>
<td colspan=1 align=left nowrap> Data described by a mime descriptor. </td></tr>
<tr valign=top>
<td colspan=1 align=left nowrap>

<p>
    </td></tr>
</table>
    <p><center>Table 9: Frame Format (<tt>SANE_Frame</tt>)</center>
    
  </center>
<p>

<p>The <tt>flags</tt> member is a 32 bit bitfield, for which up to now 4 
informational bits are defined, all unused bits have to be set to 0: 

<p><ul>

<p><li>
<tt>SANE_PFLAG_LAST_FRAME</tt> (bit 0, bitvalue 1) is set to 1 if and
only if the frame that is currently being acquired (or the frame that
will be acquired next if there is no current frame) is the last frame
of a multi frame image (e.g., the current frame is the blue component
of a red, green, blue image). Note, that it is possible to transmit
multiple images in succession.

<p><li>
<tt>SANE_PFLAG_MORE_IMAGES</tt> (bit 1, bitvalue 2) is set to 1 to indicate 
further pending images. It is permissible to set that value to 1 "in good 
faith", as it has to be determined at a very early time, where it might 
not be detectable, if there actually are more images to transfer. E.g. 
you will usually not know if the document feeder contains further pages 
when starting to scan the current one. Thus you are allowed to set that 
bit but later fail at <tt>sane_start()</tt>.

<p><li>
<tt>SANE_PFLAG_NEW_PAGE</tt> (bit 2, bitvalue 4) is set to 1 to indicate 
that the current frame comes from a new physical page. This bit is of 
informational character only to help frontends to group multi-image 
scans. 

<p><li>
<tt>SANE_PFLAG_BACKSIDE</tt> (bit 3, bitvalue 8) tells if the current image 
was acquired from the front (0) or backside (1) of the currently processed 
sheet. It is of informational character and allows to group and order 
multi-image transfers regardless of scanner acquisition order (front 
first/back first). 

<p></ul>

<p>Note, that <tt>flags</tt> is compatible to member <tt>last_frame</tt> of 
<tt>SANE_Parameters</tt> of SANE standard version 1 (same size
and only bit 0 (bitvalue 1) was used with same function).

<p>Member <tt>lines</tt> specifies how many scan lines the frame is
comprised of.  If this value is -1, the number of lines is not known a
priori and the frontend should call <tt>sane_read()</tt> until it
returns a status of <tt>SANE_STATUS_EOF</tt>.
Note, that even when transferring formats that have this information 
inband, it is recommended to set that member, if available. If 
unavailable or not applicable, set to -1 as mentioned above. 

<p>Member <tt>bytes_per_line</tt> specifies the number of bytes that
comprise one scan line.
If <tt>bytes_per_line</tt> is set to 0, which can currently only be the case for 
<tt>SANE_FRAME_MIME</tt>, the frontend shall not assume a constant line 
length. Instead it should simply try to read until <tt>SANE_STATUS_EOF</tt>
with an arbitrary block length.

<p>Member <tt>depth</tt> specifies the number of bits per sample.
Note, that only 0 (for not applicable), 1, and n*8 are allowed 
values. Data with other depths has to be scaled up accordingly. 

<p>Member <tt>pixels_per_line</tt> specifies the number of pixels that
comprise one scan line.

<p>Assume B is the number of channels in the frame, then the bit depth
d (as given by member <tt>depth</tt>) and the number of pixels per
line n (as given by this member <tt>pixels_per_line</tt>) are
related to c, the number of bytes per line (as given by member
<tt>bytes_per_line</tt>) as follows:

<p>
<center>

  c &gt;= \left{
  ll
    \lceil B*n / 8\rceil  if d=1
</td><td align=right>(1)</td></tr><tr><td align=center>

    B*n *\lceil (d + 7)/8 \rceil  if d&gt;1
  
  \right.
</center>
<p>
Note that the number of bytes per line can be larger than the minimum
value imposed by the right side of this equation.  A frontend must be
able to properly cope with such ``padded'' image formats.

<p>Member <tt>channels_per_image</tt> specifies the number of channels the
image consists of. When the image is transmitted in more than one frame
<tt>channels_per_image</tt> has to be the same for all frames for this image.

<p>Member <tt>format_desc</tt> is used for the new frametypes 
<tt>SANE_FRAME_RAW</tt> and <tt>SANE_FRAME_MIME</tt>. Its meaning differs 
between the two types: 

<p><ul>
<li>
<tt>SANE_FRAME_RAW</tt>: 
The <tt>format_desc</tt> contains a description of the channel data and
an optional depth information separated by a colon(:).

<p>A plane is descibed by one channel, e.g. "<tt>gray</tt>" or "<tt>gray:12</tt>".

<p>Channel interleaved data is described by a comma separated list of channel descriptions,
for example "<tt>red,green,blue</tt>" or "<tt>red:8,green:8,blue:8</tt>",
the channel data is sent in the given order.

<p>The depth information does <b>not</b> define the size of the transmitted
channel data, it is only an information for the frontend. The channel data has
to be sent in the size defined by member <tt>depth</tt>.

<p>Well known channels are <tt>red</tt>, <tt>green</tt>, <tt>blue</tt> and <tt>gray</tt>.
It also is allowed to use other channel descriptions, e.g. if you
use an infrared camera or scanner it could be <tt>infrared</tt> or
a wavelength description like <tt>1100nm</tt>, but be aware that a
frontend may not be able to display such channels with useful colors.

<p>Note that an image can be sent in single planes, in one interleaved
frame that contains all channels or in several frames that contain
one or more (interleaved) channels. When an RGB image is sent it
is prefered to send the image data in one interleaved frame
that consist of red, green and blue data in this order.
The number of channels is defined in member <tt>channels_per_image</tt>.

<p><li>
<tt>SANE_FRAME_MIME</tt>: 
The <tt>format_desc</tt> contains the MIME type/subtype *(;parameter) 
fields as described in RFC 1521, 4. The Content-Type header field, 
without the prefixing "Content-Type:". 
Note, that it is discouraged to transfer proprietary file formats over 
SANE. If at all possible, please stick to the IANA assigned MIME types, 
and make sure the data stream is compliant with the corresponding 
specification. 
When data is transmitted with the frame type <tt>SANE_FRAME_MIME</tt>
all data has to be transmitted within one frame, multiple frames
are not allowed (so the flag <tt>last_frame</tt> has to be set
when using this frame type).
A fully compliant SANE backend is required to transmit in either 
SANE native frametypes, or in a MIME type, for which a converting 
meta backend exists and is freely available for all platforms. 

<p>Other formats may be transmitted, but the only thing the average 
frontend can do with them, is save them. This is not considered a 
good option, as it does not facilitate transmitting the data to a 
client application that may be running the frontend. 
However, if the data transferred by the backend is not an image in 
nature, it wouldn't make sense to try converting it anyway, so it is 
acceptable to use a simple <tt>SANE_FRAME_MIME</tt> transfer for that 
case. But even then, try to stick to well known stuff with freely 
existing standards and viewers as well. 
</ul>

<p>The member <tt>proposed_filename</tt> can be used to suggest a reasonable 
default filename or -extension in case the backend can make such a 
suggestion, like e.g. an image database. 
If no such suggestion is intended, set the field to "". 

<p>In the case of raw frames, <tt>proposed_filename</tt> is expected to hold
the basename for the image, with the extension determined by the save function
of the frontend, as the frontend can fully understand the data and is thus
able to encode it in any format it wishes.

<p>For MIME frames <tt>proposed_filename</tt> can contain either: 

<p><ul>
<li>
A name with a leading dot, which is considered to be a proposed 
filename extension. This could also be gotten from the mime database, 
but for systems lacking it, this might be convenient. Or: 

<p><li>
A complete filename, including extension. 
</ul>

<p>Note, that for frontends that are able to parse a given MIME type 
internally, it is perfectly permissible to ignore the extension 
part of the proposed filename and only make use of the basename, 
when using internal save algorithms for different formats. 

<p>In any case, if the frontend makes use of this field, the frontend 
must mangle this proposal or the final filename it produces with 
its help to suit local filesystem restrictions. 

<p>Special care should be taken not to cause security flaws this way. 
For Unix, that means killing out all path separators (/) [to avoid 
to save away stuff in obscure places or create critical files like 
/etc/hosts.allow] and avoiding to overwrite existing files. 
(Creating of leading dot files - like .rhosts - is not an issue here, 
because that's only a proposed filename extension as mentioned above.

<p>The string <tt>proposed_comment</tt> can be used to transmit additional 
image data, that can be stored in the comment areas several fileformats 
offer. It can contain any textual information the backend wishes to 
convey to the user, like date/time of exposure, enganged filters, 
etc. 

<p>The members <tt>dpi_x, dpi_y</tt> encode the horizontal and vertical 
resolution. Note, that multiple-image scans may have different
resolutions of each image. It is not permissible to change resolution 
between frames of the same image.

<p>The member <tt>reserved</tt> is an array of 32 bytes (char) to keep
the size of the struct unchanged when future extensions are done.
The backend has to set the reserved bytes to 0.
</font>

<p><h3><a name="s4.3.9">4.3.9 <tt>sane_start</tt></a></h3>

<p>This function initiates aquisition of an image from the device
represented by handle <tt>h</tt>.
<blockquote><a name="i106">
<pre>
SANE_Status sane_start (SANE_Handle h);
</pre>
</blockquote>
This function may fail with one of the following status codes.
<blockquote>
<dl>
<dt><tt>SANE_STATUS_CANCELLED</tt>:<dd> The operation was cancelled through
  a call to <tt>sane_cancel</tt>.
<dt><tt>SANE_STATUS_DEVICE_BUSY</tt>:<dd> The device is busy. The
  operation should be retried later.
<dt><tt>SANE_STATUS_JAMMED</tt>:<dd> The document feeder is jammed.
<dt><tt>SANE_STATUS_NO_DOCS</tt>:<dd> The document feeder is out of
  documents.
<dt><tt>SANE_STATUS_COVER_OPEN</tt>:<dd> The scanner cover is open.
<dt><tt>SANE_STATUS_IO_ERROR</tt>:<dd> An error occurred while communicating
  with the device.
<dt><tt>SANE_STATUS_NO_MEM</tt>:<dd> An insufficent amount of memory
  is available.
<dt><tt>SANE_STATUS_INVAL</tt>:<dd> The scan cannot be started with the current
  set of options. The frontend should reload the option descriptors, as if
  <tt>SANE_INFO_RELOAD_OPTIONS<a name="i107"></tt> had been returned from a call to
  <tt>sane_control_option()</tt>, since the device's capabilities may have
  changed.
</dl>
</blockquote>

<p><h3><a name="s4.3.10">4.3.10 <tt>sane_read</tt></a></h3>

<p>This function is used to read image data from the device represented
by handle <tt>h</tt>.  Argument <tt>buf</tt> is a pointer to a memory area
that is at least <tt>maxlen</tt> bytes long.  The number of bytes
returned is stored in <tt>*len</tt>.  A backend must set this to zero
when a status other than <tt>SANE_STATUS_GOOD</tt> is returned).
When the call succeeds, the number of bytes returned can be anywhere in
the range from 0 to <tt>maxlen</tt> bytes.
<blockquote><a name="i108">
<pre>
SANE_Status sane_read (SANE_Handle h, SANE_Byte * buf,
                       SANE_Int maxlen, SANE_Int * len);
</pre>
</blockquote>
For efficiency reasons, medium to large 
block sizes (in the range of a few kilobytes) should be used. 
Returning short reads is allowed to allow for small buffers 
in the backend. 
If this function is called when no data is available, one of two
things may happen, depending on the I/O mode that is in effect for
handle <tt>h</tt>.
<ol>
<li>If the device is in blocking I/O mode (the default mode), the
  call blocks until at least one data byte is available (or until some
  error occurs).

<p><li>If the device is in non-blocking I/O mode, the call returns
  immediately with status <tt>SANE_STATUS_GOOD</tt> and with
  <tt>*len</tt> set to zero.
</ol>
The I/O mode of handle <tt>h</tt> can be set via a call to
<tt>sane_set_io_mode()</tt>.

<p>This function may fail with one of the following status codes.
<blockquote>
<dl>
<dt><tt>SANE_STATUS_CANCELLED</tt>:<dd> The operation was cancelled through
  a call to <tt>sane_cancel</tt>.
<dt><tt>SANE_STATUS_EOF</tt>:<dd> No more data is available for the
  current frame. If <tt>sane_read</tt> sends back any image data it
is not allowed to return with <tt>SANE_STATUS_EOF</tt>.
<dt><tt>SANE_STATUS_JAMMED</tt>:<dd> The document feeder is jammed.
<dt><tt>SANE_STATUS_NO_DOCS</tt>:<dd> The document feeder is out of
  documents.
<dt><tt>SANE_STATUS_COVER_OPEN</tt>:<dd> The scanner cover is open.
<dt><tt>SANE_STATUS_IO_ERROR</tt>:<dd> An error occurred while communicating
  with the device.
<dt><tt>SANE_STATUS_NO_MEM</tt>:<dd> An insufficent amount of memory
  is available.
<dt><tt>SANE_STATUS_ACCESS_DENIED</tt>:<dd> Access to the device has
  been denied due to insufficient or invalid authentication.
</dl>
</blockquote>

<p><h3><a name="s4.3.11">4.3.11 <tt>sane_cancel</tt></a></h3>

<p>This function is used to immediately or as quickly as possible cancel
the currently pending operation of the device represented by handle
<tt>h</tt>.
<blockquote><a name="i109">
<pre>
void sane_cancel (SANE_Handle h);
</pre>
</blockquote>
This function can be called at any time (as long as handle <tt>h</tt> is
a valid handle) but usually affects long-running operations only (such
as image is acquisition).  It is safe to call this function
asynchronously (e.g., from within a signal handler).  It is important
to note that completion of this operaton does <em>not</em> imply that
the currently pending operation has been cancelled.  It only
guarantees that cancellation has been <em>initiated</em>.  Cancellation
completes only when the cancelled call returns (typically with a
status value of <tt>SANE_STATUS_CANCELLED</tt>).  Since the SANE API
does not require any other operations to be re-entrant, this implies
that a frontend must <em>not</em> call any other operation until the
cancelled operation has returned.

<p><h3><a name="s4.3.12">4.3.12 <tt>sane_set_io_mode</tt></a></h3>

<p>This function is used to set the I/O mode of handle <tt>h</tt>.  The I/O mode
can be either blocking or non-blocking.  If argument <tt>m</tt> is
<tt>SANE_TRUE</tt>, the mode is set to non-blocking mode, otherwise it's set to
blocking mode.  This function can be called only after a call to
<tt>sane_start()</tt> has been performed.
<blockquote><a name="i110">
<pre>
SANE_Status sane_set_io_mode (SANE_Handle h, SANE_Bool m);
</pre>
</blockquote>
By default, newly opened handles operate in blocking mode.  A backend
may elect not to support non-blocking I/O mode.  In such a case the
status value <tt>SANE_STATUS_UNSUPPORTED</tt> is returned.  Blocking
I/O must be supported by all backends, so calling this function with
argument <tt>m</tt> set to <tt>SANE_FALSE</tt> is guaranteed to complete
successfully.

<p>This function may fail with one of the following status codes:
<blockquote>
<dl>
<dt><tt>SANE_STATUS_INVAL</tt>:<dd> No image acquisition is pending.
<dt><tt>SANE_STATUS_UNSUPPORTED</tt>:<dd> The backend does not support
the requested I/O mode.
</dl>
</blockquote>

<p><h3><a name="s4.3.13">4.3.13 <tt>sane_get_select_fd</tt></a></h3>

<p>This function is used to obtain a (platform-specific) file-descriptor
for handle <tt>h</tt> that is readable if and only if image data is
available (i.e., when a call to <tt>sane_read()</tt> will return at
least one byte of data).  If the call completes successfully, the
select file-descriptor is returned in <tt>*fd</tt>.
<blockquote><a name="i111">
<pre>
SANE_Status sane_get_select_fd (SANE_Handle h, SANE_Int *fd);
</pre>
</blockquote>
This function can be called only after a call to <tt>sane_start()</tt>
has been performed and the returned file-descriptor is guaranteed to
remain valid for the duration of the current image acquisition (i.e.,
until <tt>sane_cancel()</tt> or <tt>sane_start()</tt> get called again
or until <tt>sane_read()</tt> returns with status
<tt>SANE_STATUS_EOF</tt>).  Indeed, a backend must guarantee to
close the returned select file descriptor at the point when the next
<tt>sane_read()</tt> call would return <tt>SANE_STATUS_EOF</tt>.
This is necessary to ensure the application can detect when this
condition occurs without actually having to call <tt>sane_read()</tt>.

<p>A backend may elect not to support this operation.  In such a case,
the function returns with status code
<tt>SANE_STATUS_UNSUPPORTED</tt>.

<p>Note that the only operation supported by the returned file-descriptor
is a host operating-system dependent test whether the file-descriptor
is readable (e.g., this test can be implemented using <tt>select()</tt>
or <tt>poll()</tt> under UNIX).  If any other operation is performed on
the file descriptor, the behavior of the backend becomes
unpredictable.  Once the file-descriptor signals ``readable'' status,
it will remain in that state until a call to <tt>sane_read()</tt> is
performed.  Since many input devices are very slow, support for this
operation is strongly encouraged as it permits an application to do
other work while image acquisition is in progress.

<p>This function may fail with one of the following status codes:
<blockquote>
<dl>
<dt><tt>SANE_STATUS_INVAL</tt>:<dd> No image acquisition is pending.
<dt><tt>SANE_STATUS_UNSUPPORTED</tt>:<dd> The backend does not support
  this operation.
</dl>
</blockquote>

<p><h3><a name="s4.3.14">4.3.14 <tt>sane_strstatus</tt></a></h3>

<p>This function can be used to translate a SANE status code into a
printable string.  The returned string is a single line of text that
forms a complete sentence, but without the trailing period
(full-stop).  The function is guaranteed to never return <tt>NULL</tt>.
The returned pointer is valid at least until the next call to this
function is performed.
<blockquote><a name="i112">
<pre>
const SANE_String_Const sane_strstatus (SANE_Status status);
</pre>
</blockquote>

<p><p><hr>
<a href="doc013.html"><img src=../icons/next.gif alt="Next"></a>
<a href="doc000.html"><img src=../icons/up.gif alt="Up"></a>
<a href="doc011.html"><img src=../icons/previous.gif alt="Previous"></a>
<a href="doc000.html"><img src=../icons/contents.gif alt="Contents"></a>
<a href="doc019.html"><img src=../icons/index.gif alt="Index"></a>
<hr>
</body></html>
