<html><body>
<a href="doc009.html"><img src=../icons/next.gif alt="Next"></a>
<a href="doc000.html"><img src=../icons/up.gif alt="Up"></a>
<a href="doc007.html"><img src=../icons/previous.gif alt="Previous"></a>
<a href="doc000.html"><img src=../icons/contents.gif alt="Contents"></a>
<a href="doc019.html"><img src=../icons/index.gif alt="Index"></a>
<hr>

<title>Image Data Format</title>
<h2><a name="s3.2">3.2 Image Data Format</a></h2><a name="i0">

<p>Arguably the most important aspect of an image acquisition system is how
images are represented. The SANE approach is to define a simple yet powerful
representation that is sufficient for vast majority of applications and
devices. While the representation is simple, the interface has been defined
carefully to allow extending it in the future without breaking backwards
compatibility. Thus, it will be possible to accommodate future applications or
devices that were not anticipated at the time this standard was created.

<p>A SANE image is a rectangular area.  The rectangular area is subdivided into a
number of rows and columns.  At the intersection of each row and column is a
(preferable quadratic) pixel. A pixel consists of one or more sample values.
Each sample value represents one channel (e.g., the red channel).

<p>The SANE API transmits an image as a sequence of frames.  Each frame covers
the same rectangular area as the entire image, but may contain only a subset
of the channels in the final image.  For example, a red/green/blue image could
either be transmitted as a single frame that contains the sample values for
all three channels or it could be transmitted as a sequence of three frames:
the first frame containing the red channel, the second the green channel, and
the third the blue channel.

<p>When transmitting an image frame by frame, the frontend needs to know what
part of the image a frame represents (and how many frames it should expect).
For that purpose, the SANE API tags every frame with a type and a format
descriptor.

<p><font color="darkgreen">
  There are two different types of frames: pixel oriented frames
  <tt>SANE_FRAME_RAW<a name="i1"></tt> and arbitrary data frames
  <tt>SANE_FRAME_MIME<a name="i2"></tt>. These types are discussed in detail in the
  following sections. The frame types used by version 1 of this standard
  (<tt>SANE_FRAME_GRAY<a name="i3"></tt>, <tt>SANE_FRAME_RGB<a name="i4"></tt>,
  <tt>SANE_FRAME_RED<a name="i5"></tt>, <tt>SANE_FRAME_GREEN<a name="i6"></tt>, and
  <tt>SANE_FRAME_BLUE<a name="i7"></tt>) are obsolete and superseded by
  <tt>SANE_FRAME_RAW<a name="i8"></tt>.

<p>  <h3><a name="s3.2.1">3.2.1 Pixel oriented frames</a></h3>

<p>  The type of pixel oriented frames is <tt>SANE_FRAME_RAW<a name="i9"></tt>. The
  frame contains one or more channels of data in a channel-interleaved format,
  that represents sample values from a property of the individual pixels that
  is subject to further description in the <tt>format_desc</tt> member of the
  <tt>SANE_Parameters</tt> structured type. See section <a href="doc012.html#s4.3.8">4.3.8</a>
  on page <a href="doc012.html#s4.3.8">4.3.8</a> for details about the format
  descriptions.
</font>

<p>  Each sample value has a certain bit depth.  The bit depth is fixed for the
  entire image and can be as small as one bit. Valid bit depths are 1, 8, or
  16 bits per sample. If a device's natural bit depth is something else, it is
  up to the driver to scale the sample values appropriately (e.g., a 4 bit
  sample could be scaled by a factor of four to represent a sample value of
  depth 8).

<p><font color="darkgreen">
  The complete image may consist of several different channels. The number of channels
  is defined by member <tt>channels_per_image</tt> of <tt>SANE_Parameters</tt>.
  The image may be transmitted in an arbitary number of frames which can be
  determined by watching the <tt>SANE_PFLAG_LAST_FRAME</tt> flag in said type (or by
  counting the channels).  Note: This frame type replaces all frame types of
  the SANE standard version 1.
</font>

<p>Conceptually, each pixel oriented frame is transmitted a byte at a time.  Each
byte may contain 8 sample values (for an image bit depth of 1), one full
sample value (for an image bit depth of 8), or a partial sample value (for an
image bit depth of 16 or bigger).  In the latter case, the bytes of each
sample value are transmitted in the machine's native byte order.
<blockquote>
  <center>
    <b>Backend Implementation Note</b>
  </center>
  A network-based meta backend will have to ensure that the byte order
  in image data is adjusted appropriately if necessary.  For example,
  when the meta backend attaches to the server proxy, the proxy may
  inform the backend of the server's byte order.  The backend can then
  apply the adjustment if necessary.  In essence, this implements a
  ``receiver-makes-right'' approach.
</blockquote>

<p><p><a name="f2"></a>
  <center>
    
    <img width=390 height=196 src="img001.gif">
    <p><center>Figure 2: Transfer order of image data bytes</center>
    
  </center>
<p>

<p>The order in which the sample values in a frame are transmitted is illustrated
in Figure <a href="doc008.html#f2">2</a>.  As can be seen, the values are transmitted row by
row and each row is transmitted from left-most to right-most column.  The
left-to-right, top-to-bottom transmission order applies when the image is
viewed in its normal orientation (as it would be displayed on a screen, for
example).

<p>If a frame contains multiple channels, then the channels are transmitted in an
interleaved fashion.  Figure <a href="doc008.html#f3">3</a> illustrates this for the case
where a frame contains a complete red/green/blue image with a bit-depth of 8.

<p><p><a name="f3"></a>
  <center>
    
    <img width=624 height=111 src="img002.gif">
    <p><center>Figure 3: Bit and byte order of image data</center>
    
  </center>
<p>

<p>For a bit depth of 1, each byte contains 8 sample values of a <em>single</em>
channel. In other words, a bit depth 1 frame is transmitted in a byte
interleaved fashion. The first sample of each byte is represented by the most
significant bit.

<p><font color="darkgreen">
For gray channels at a bit depth of 1 only two sample values are possible: 1
represents minimum intensity (black) and 0 represents maximum intensity
(white).  For all other channel types and bit depths a sample value of 0
represents minimum intensity and larger values represent increasing intensity.

<p><h3><a name="s3.2.2">3.2.2 Arbitrary data frames</a></h3>

<p>It also is possible to transmit arbitrary (not necessaryly pixel oriented)
data. This allows transmission of compressed images like jpeg, tiff, etc.

<p>The type of arbitrary data frames is <tt>SANE_FRAME_MIME<a name="i10"></tt>.
The frame contains arbitrary data of the MIME (see RFC 1521/1522) type that is
given in the <tt>format_desc</tt> member of the <tt>SANE_Parameters</tt>
structured type (see See section <a href="doc012.html#s4.3.8">4.3.8</a> on
page <a href="doc012.html#s4.3.8">4.3.8</a>).  As such, it is assumed to be
incomprehensible to the frontend, except for selected types the frontend is
specifically capable of handling internally.  The frontend is free to ignore
those frames, or employ any appropriate means to otherwise handle this data
(like saving them to disk or spawning an external viewer).
</font>

<p><p><hr>
<a href="doc009.html"><img src=../icons/next.gif alt="Next"></a>
<a href="doc000.html"><img src=../icons/up.gif alt="Up"></a>
<a href="doc007.html"><img src=../icons/previous.gif alt="Previous"></a>
<a href="doc000.html"><img src=../icons/contents.gif alt="Contents"></a>
<a href="doc019.html"><img src=../icons/index.gif alt="Index"></a>
<hr>
</body></html>
